import React from 'react';
import Svg, {Defs, LinearGradient, Stop, Path} from 'react-native-svg';

function LogoSvg(props) {
  return (
    <Svg
      xmlns="http://www.w3.org/2000/svg"
      width={props.style.width}
      height={props.style.height}
      viewBox="0 0 94.556 49.621"
      {...props}>
      <Defs>
        <LinearGradient
          id="prefix__a"
          x1={-0.028}
          y1={0.527}
          x2={2.88}
          y2={0.415}
          gradientUnits="objectBoundingBox">
          <Stop offset={0} stopColor="#009540" />
          <Stop offset={1} stopColor="#93c01f" />
        </LinearGradient>
      </Defs>
      <Path
        data-name="Path 136"
        d="M178.543 67.594a15.911 15.911 0 00-7.684 1.971l-11.23-16.135h.923a1.715 1.715 0 000-3.43h-11.325a1.715 1.715 0 100 3.43h6.1l-13.363 19.2-8.068-11.593h.649a1.715 1.715 0 000-3.43h-11.329a1.715 1.715 0 100 3.43h6.278l-5.835 8.384c-.021.03-.034.063-.053.094a16.152 16.152 0 102.805 1.939c.02-.026.044-.047.063-.074l5.219-7.5 13.432 19.3a1.715 1.715 0 002.387.428 1.715 1.715 0 00.428-2.388l-3.89-5.589 13.426-19.29 10.577 15.2a15.988 15.988 0 1010.49-3.944zM128.6 83.608a12.583 12.583 0 11-12.583-12.584A12.6 12.6 0 01128.6 83.608zm49.946 12.583a12.583 12.583 0 1112.583-12.583 12.6 12.6 0 01-12.586 12.583z"
        transform="translate(-100 -50)"
        fill="url(#prefix__a)"
      />
    </Svg>
  );
}

export default LogoSvg;
