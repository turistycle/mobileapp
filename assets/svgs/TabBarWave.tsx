import * as React from "react"
import Svg, { Path } from "react-native-svg"

const { tabBarColor } = require('../scss/settings/colors.scss')
function TabBarWave(props) {
  return (
    <Svg
      xmlns="http://www.w3.org/2000/svg"
      width={props.style.width}
      height={16}
      viewBox="0 0 407.707 77.81"
      {...props}
    >
      <Path
        data-name="Path 446"
        d="M0 .051c12.581.272 55.311 2.5 92.758 32.128 40.574 25.25 51.438 41.6 107.784 42.371 56.461-1.341 70.017-13.016 114.082-42.371C359.53-2.7 407.707.051 407.707.051V77.81H0z"
        fill={tabBarColor.color}
      />
    </Svg>
  )
}

export default TabBarWave;
