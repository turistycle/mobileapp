import * as React from 'react';
import {useEffect, useState} from 'react';
import {View} from 'react-native';
import {connect} from 'react-redux';
import Snackbar from 'react-native-snackbar';

const Alerts = ({message}) => {
  const [isFirstCall, setIsFirstCall] = useState<boolean>(true);

  useEffect(() => {
    if (isFirstCall) {
      setIsFirstCall(false);
    } else {
      try {
        if (message.err !== null)
          Snackbar.show({
            text: `${message.err}`,
            textColor: '#c44141',
            duration: 3000,
          });
        else {
          Snackbar.show({
            text: `${message.msg}`,
            textColor: '#41c452',
            duration: 3000,
          });
        }
      } catch (err) {
        console.log({err});
      }
    }
  }, [message]);

  return <View/>;
};

const mapStateToProps = state => ({
  message: state.messages,
});

export default connect(mapStateToProps)(Alerts);
