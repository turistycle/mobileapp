import * as React from 'react';
import {useEffect, useState} from 'react';
import {Animated, Easing, SafeAreaView, Text, TouchableOpacity, View,} from 'react-native';
import {connect} from 'react-redux';
import Icon from '../components/Icon';
import {showNotification} from '../actions/notifications';

const styles = require('../../assets/scss/alerts/pushnotifications.scss');

function InnerNotification({
                             user,
                             notification,
                             currentConversation,
                             showNotification,
                           }) {
  const animated = new Animated.Value(-200);
  const duration = 1000;
  const [lastNotification, setLastNotification] = useState({});
  useEffect(() => {
    if (
      notification.text &&
      notification.conversationId !== currentConversation.id &&
      notification.from.uid !== user.uid &&
      JSON.stringify(notification) !== JSON.stringify(lastNotification)
    ) {
      Animated.sequence([
        Animated.timing(animated, {
          toValue: 0,
          duration,
          useNativeDriver: true,
          easing: Easing.cubic,
        }),
        Animated.timing(animated, {
          delay: 2000,
          toValue: -200,
          duration: 500,
          useNativeDriver: true,
        }),
      ]).start();
      console.log('is in');
      showNotification();
    }
  }, [notification]);

  const hideNotification = () => {
    Animated.sequence([
      Animated.timing(animated, {
        toValue: -200,
        duration: 500,
        useNativeDriver: true,
      }),
    ]).start();
  };

  return (
    <SafeAreaView
      style={{position: 'absolute', zIndex: 5, elevation: 5, width: '100%'}}>
      <View style={{position: 'relative', width: '100%'}}>
        <Animated.View
          style={{...styles.push, transform: [{translateY: animated}]}}>
          <View style={styles.push__inner}>
            <View style={styles.push__inner__1}>
              <Text style={styles.push__sender}>
                {notification?.from.displayName}
              </Text>
              <Text style={styles.push__message}>
                {notification.text?.replace(
                  /%!@#\$\^&\*\(\)%[0-9.,-]*%!@#\$\^&\*\(\)%/g,
                  '',
                )}
              </Text>
            </View>
            <View style={styles.push__inner__2}>
              <TouchableOpacity
                style={styles.push__dismiss}
                onPress={hideNotification}>
                <Icon name="x" style={styles.push__dismissIcon} size={20}/>
              </TouchableOpacity>
            </View>
          </View>
        </Animated.View>
      </View>
    </SafeAreaView>
  );
}

const mapStateToProps = state => ({
  notification: state.notifications,
  currentConversation: state.chat.currentConversation,
  user: state.auth.user,
});

export default connect(mapStateToProps, {showNotification})(InnerNotification);
