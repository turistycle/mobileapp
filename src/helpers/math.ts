export const getRoundedAvg = arr => {
  const trueAvg = [...arr].reduce((a,b) => a + b, 0) / arr.length
  return Math.round(trueAvg / 0.5) * 0.5
}