export const filterExternalPoints = (x, _, list) => {
  const isLimit =
    list.filter(y => JSON.stringify(y.location) === JSON.stringify(x.location))
      .length === 2;
  if (!isLimit) {
    return true;
  } else return !!(isLimit && x._id);
};
export const pointInTwoLists = (list1, list2, point) => {
  const score1 = list1.find(
    y => JSON.stringify(y.location) === JSON.stringify(point.location),
  );
  if (score1) {
    const score2 = list2.find(
      y => JSON.stringify(y.location) === JSON.stringify(point.location),
    );
    if (score2) {
      return true;
    }
  }
  return false;
};
