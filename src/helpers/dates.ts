const moment = require('moment');

export const messageDate = date => {
  return moment(date).format('MMMM Do, h:mm a');
};

export const postDate = date => {
  return moment(date).format('MM/DD, h:mm a');
};

export const postTime = date => {
  const duration = moment.duration(date * 1000);
  const hours = `${duration.hours() ? duration.hours() + 'h ' : ''}`
  const minutes = `${duration.minutes() ? duration.minutes() + 'min ' : ''}`
  const seconds = `${duration.seconds() ? duration.seconds() + 's' : ''}`
  return `${hours}${minutes}${seconds}`
}

export const searchTripDate = date => {
  return moment(date).format(' Do MMMM YYYY');
};
