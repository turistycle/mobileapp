import {NEW_NOTIFICATION,} from '../common/types';

const initialState = {
  from: {uid: '', photoURL: '', displayName: ''},
  text: '',
  conversationId: '',
};
export default (state = initialState, action) => {
  switch (action.type) {
    case NEW_NOTIFICATION:
      return (state = {...state, ...action.payload});
    default:
      return state;
  }
};
