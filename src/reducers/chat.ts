import {
  RESET_CONVERSATIONS,
  RESET_CURRENT_CONVERSATION,
  SET_CURRENT_CONVERSATION,
  UPDATE_CHAT_MESSAGES,
  UPDATE_CONVERSATIONS,
  UPDATE_CONVERSATIONS_LAST_MESSAGE,
} from '../common/types';

const initialState = {
  currentConversation: {id: null, messages: []},
  conversations: [],
};
export default (state = initialState, action) => {
  switch (action.type) {
    case SET_CURRENT_CONVERSATION:
      return (state = {
        ...state,
        currentConversation: {...state.currentConversation, id: action.payload},
      });
    case UPDATE_CONVERSATIONS_LAST_MESSAGE:
      const conversations = state.conversations.map(item => {
        if (item.id === action.payload.id) {
          item.lastMessage = action.payload.lastMessage;
        }
        return item;
      });
      return (state = {...state, conversations});
    case UPDATE_CONVERSATIONS:
      return (state = {
        ...state,
        conversations: [...state.conversations, action.payload],
      });
    case RESET_CURRENT_CONVERSATION:
      return (state = {
        ...state,
        currentConversation: initialState.currentConversation,
      });
    case RESET_CONVERSATIONS:
      return (state = initialState);
    case UPDATE_CHAT_MESSAGES:
      const messages = [...state.currentConversation.messages, action.payload]
        .sort((a, b) => new Date(b.time) - new Date(a.time));

      messages.forEach((item, index) => {
        if (index !== messages.length - 1) {
          const time = {
            previous: messages[index + 1].time,
            current: item.time,
          };

          const difference = time.current - time.previous;
          item['isDateShow'] = difference >= 1800000;
        } else {
          item['isDateShow'] = true;
        }
      });
      return (state = {
        ...state,
        currentConversation: {...state.currentConversation, messages},
      });
    default:
      return state;
  }
};
