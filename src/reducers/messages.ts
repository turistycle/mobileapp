import {GOT_ERROR, GOT_MESSAGE} from '../common/types';

const initialState = {};
export default function (state = initialState, action) {
  switch (action.type) {
    case GOT_ERROR:
      return {err: action.payload, msg: null}
    case GOT_MESSAGE:
      return {err: null, msg: action.payload}
    default:
      return state;
  }
}
