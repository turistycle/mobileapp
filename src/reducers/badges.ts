import {RESET_NOTIFICATION, SHOW_NOTIFICATION} from '../common/types';

const initialState = {
  chatBadge: false,
};
export default (state = initialState, action) => {
  switch (action.type) {
    case SHOW_NOTIFICATION:
      return (state = {...state, chatBadge: true});
    case RESET_NOTIFICATION:
      return (state = {...state, chatBadge: false});
    default:
      return state
  }
};
