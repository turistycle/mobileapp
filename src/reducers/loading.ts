import {IS_LOADING, IS_NOT_LOADING} from '../common/types';

const initialState = false;
export default function (state = initialState, action) {
  switch (action.type) {
    case IS_LOADING:
      return (state = true);
    case IS_NOT_LOADING:
      return (state = false);
    default:
      return state;
  }
}
