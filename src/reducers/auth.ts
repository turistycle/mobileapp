import {LOG_IN, LOG_OUT} from '../common/types';
import {FirebaseAuthTypes} from "@react-native-firebase/auth";

interface Auth {
  user: FirebaseAuthTypes.User | {}
}

const initialState: Auth = {
  user: {},
};

export default function (state: Auth = initialState, action) {
  switch (action.type) {
    case LOG_IN:
      return (state = action.payload);
    case LOG_OUT:
      return (state = initialState);
    default:
      return state;
  }
}
