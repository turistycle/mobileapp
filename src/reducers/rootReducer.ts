import {combineReducers} from 'redux';
import auth from './auth';
import loading from './loading';
import messages from './messages';
import chat from './chat';
import notifications from './notifications';
import badges from "./badges";

export default combineReducers({
  auth,
  loading,
  messages,
  chat,
  notifications,
  badges
});
