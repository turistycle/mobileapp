import * as React from 'react';
import {connect} from 'react-redux';
import MyView from '../components/MyView';
import {
  Image,
  ScrollView,
  TouchableOpacity,
  View,
  Dimensions,
} from 'react-native';
import MyText from '../components/MyText';
import Card from '../components/Card';
import Icon from '../components/Icon';
import {LineChart} from 'react-native-chart-kit';
import {LineChartData} from 'react-native-chart-kit/dist/line-chart/LineChart';
import {AbstractChartConfig} from 'react-native-chart-kit/dist/AbstractChart';

const styles = require('../../assets/scss/pages/mainpage.scss');

function MainScreen({chatBadge, user, navigation}) {
  const data: LineChartData = {
    labels: ['Aug', 'Sep', 'Oct', 'Nov', 'Dec'],
    datasets: [
      {
        data: [20, 19, 14, 100],
        color: (opacity = 1) => styles.chart__main.color,
        strokeWidth: 2, // optional
        withDots: false,
      },
    ],
    legend: ['Kilometers per month'], // optional
  };
  const chartConfig: AbstractChartConfig = {
    decimalPlaces: 0,
    backgroundGradientFrom: 'white',
    backgroundGradientFromOpacity: 1,
    backgroundGradientTo: 'white',
    backgroundGradientToOpacity: 1,
    color: (opacity = 1) => styles.chart__stroke.color,
    useShadowColorFromDataset: true, // optional,
    propsForBackgroundLines: {
      strokeDasharray: [600],
      stroke: '#e5e5e5',
      strokeWidth: 1,
    },
  };
  return (
    <MyView
      title="Dashboard"
      withoutMargin={true}
      ButtonRight={({style}) => (
        <TouchableOpacity
          onPress={() => navigation.navigate('ChatNav')}
          style={style.header__button}>
          {chatBadge && <View style={styles.badge} />}
          <Icon name="chat" size={35} style={style.header__buttonIcon} />
        </TouchableOpacity>
      )}
      ButtonLeft={({style}) => (
        <TouchableOpacity
          style={style.header__button}
          onPress={() =>
            navigation.navigate(
              'SettingsNavigation',
              {},
              setTimeout(() => navigation.push('UserScreen'), 10),
            )
          }>
          <Image style={styles.imageCircle} source={{uri: user.photoURL}} />
        </TouchableOpacity>
      )}>
      <ScrollView
        contentContainerStyle={{paddingBottom: 30}}
        showsVerticalScrollIndicator={false}>
        <Card
          bodyStyle={{flexDirection: 'row'}}
          style={{width: '94%', marginHorizontal: '3%'}}
          headerText="Calories">
          <MyText>Hello World!</MyText>
        </Card>
        <ScrollView
          showsHorizontalScrollIndicator={false}
          contentContainerStyle={{
            flexDirection: 'row',
            paddingHorizontal: '3%',
          }}
          horizontal={true}>
          <Card
            bodyStyle={{flexDirection: 'row'}}
            style={{marginRight: 15}}
            headerText="Calories">
            <MyText>Hello World!</MyText>
          </Card>
          <Card
            bodyStyle={{flexDirection: 'row'}}
            style={{marginRight: 15}}
            headerText="Calories">
            <MyText>Hello World!</MyText>
          </Card>
          <Card
            style={{marginRight: 15}}
            bodyStyle={{flexDirection: 'row'}}
            headerText="Calories">
            <MyText>Hello World!</MyText>
          </Card>
          <Card
            bodyStyle={{flexDirection: 'row'}}
            style={{marginRight: 15}}
            headerText="Calories">
            <MyText>Hello World!</MyText>
          </Card>
          <Card
            bodyStyle={{flexDirection: 'row'}}
            style={{marginRight: 15}}
            headerText="Calories">
            <MyText>Hello World!</MyText>
          </Card>
          <Card bodyStyle={{flexDirection: 'row'}} headerText="Calories">
            <MyText>Hello World!</MyText>
          </Card>
        </ScrollView>
        <LineChart
          style={styles.chart}
          data={data}
          fromZero
          width={Dimensions.get('window').width * 0.94}
          height={220}
          chartConfig={chartConfig}
          withVerticalLines={false}
          bezier={true}
        />
      </ScrollView>
    </MyView>
  );
}

const mapStateToProps = state => ({
  user: state.auth.user,
  chatBadge: state.badges.chatBadge,
});

export default connect(mapStateToProps)(MainScreen);
