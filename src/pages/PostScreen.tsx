import * as React from 'react';
import {connect} from 'react-redux';
import {View} from 'react-native';
import MyText from '../components/MyText';

const PostScreen = props => {
  return (
    <View
      style={{
        borderTopLeftRadius: 12,
        borderTopRightRadius: 12,
        backgroundColor: 'whitesmoke',
        flex: 1,
      }}>
      <View
        style={{
          marginVertical: 10,
          marginHorizontal: '30%',
          height: 5,
          backgroundColor: '#494949',
          borderRadius: 5,
        }}
      />
      <MyText style={{textAlign: 'center', fontSize: 30, color: '#4c4c4c'}}>
        {props.route.params?.text ?? 'POST'}
      </MyText>
      <View style={{padding: 20, flex: 1}}>
        <View style={{marginBottom: 30}}/>
        <MyText
          style={{
            textAlign: 'center',
            fontSize: 25,
            color: '#4c4c4c',
            marginBottom: 10,
          }}>
          Comments section
        </MyText>
      </View>
    </View>
  );
};

export default connect()(PostScreen);
