import * as React from 'react';
import {
  KeyboardAvoidingView,
  ScrollView,
  TextInput,
  TouchableOpacity,
  View,
} from 'react-native';
import {connect} from 'react-redux';
import MyView from '../../../components/MyView';
import MapView from '../../../components/MapView';
import MapboxGL from '@react-native-mapbox-gl/maps';
import MyText from '../../../components/MyText';
import Icon from '../../../components/Icon';
import {useState} from 'react';
import {allFilters, initialFiltersState} from '../../../common/consts';
import Card from '../../../components/Card';
import {postTrip, sendMarkers} from '../../../actions/maps';

const styles = require('../../../../assets/scss/pages/summarytripscreen.scss');

const SummaryTripScreen = ({route, navigation, sendMarkers, postTrip}) => {
  const [formData, setFormData] = useState({
    title: '',
    categories: [],
    tags: [],
    citiesAround: [],
  });

  const {geoJson, points} = route.params;
  const coords = geoJson.features[0].geometry.coordinates;
  const ne: [number, number] = [
    Math.max(...coords.map(x => x[0])),
    Math.max(...coords.map(x => x[1])),
  ];
  const sw: [number, number] = [
    Math.min(...coords.map(x => x[0])),
    Math.min(...coords.map(x => x[1])),
  ];
  const bounds = {
    ne,
    sw,
    paddingLeft: 10,
    paddingRight: 10,
    paddingTop: 10,
    paddingBottom: 10,
  };

  const checkFilterOption = (filter, value) => {
    const check = formData[filter].find(x => x === value.value);
    if (check) {
      setFormData({
        ...formData,
        [filter]: [...formData[filter].filter(x => x !== value.value)],
      });
    } else {
      setFormData({
        ...formData,
        [filter]: [...formData[filter], value.value],
      });
    }
  };

  const handleFormInput = (e, field) => {
    switch (field) {
      case 'title':
        setFormData({...formData, title: e});
        break;
    }
  };

  const publishTrip = async () => {
    const savedPoints = await sendMarkers(
      points.map(x => ({
        name: x.name || `Point No. ${Math.floor(Math.random() * 600)}`,
        latitude: x.location[1],
        longitude: x.location[0],
      })),
    );
    await postTrip({
      name: formData.title,
      citiesAround: formData.citiesAround,
      categories: formData.categories,
      tags: formData.tags,
      markers: savedPoints.map(({_id})=> _id),
      geoJson: geoJson,
      length: points.reduce((acc = 0, x) => acc+ x.distance)
    });
    navigation.reset({index: 0, routes: [{name: 'MapScreen'}]});
  };

  return (
    <MyView
      title={'Summary'}
      withoutSafeArea
      withoutMargin
      withoutBackground
      floatingHeader
      style={{backgroundColor: 'white'}}
      ButtonLeft={({style}) => (
        <TouchableOpacity
          onPress={() => navigation.pop()}
          style={{...style.header__button, transform: [{rotate: '90deg'}]}}>
          <Icon
            name={'chevron-down'}
            size={30}
            style={style.header__buttonIcon}
          />
        </TouchableOpacity>
      )}>
      <KeyboardAvoidingView style={{flex: 1}} behavior={'padding'}>
        <ScrollView contentContainerStyle={{flexGrow: 1, paddingBottom: 40}}>
          <MapView style={styles.map} disableInteraction={false}>
            <MapboxGL.Camera
              bounds={{...bounds, paddingTop: 120}}
              animationDuration={800}
              animationMode={'flyTo'}
            />
            <MapboxGL.ShapeSource id="line" shape={geoJson}>
              <MapboxGL.LineLayer id="lineLayer" style={styles.map__line} />
            </MapboxGL.ShapeSource>
            {points.map(({location, name}, index) => (
              <MapboxGL.PointAnnotation
                // onSelected={() =>
                //   shouldDeselectPoint(index, [longitude, latitude])
                // }
                // onDeselected={() => shouldDeselectPoint()}
                snippet={'someSnippet'}
                title={name || `Point no. ${index + 1}`}
                key={index}
                id={name}
                coordinate={location}>
                <View style={styles.map__point}>
                  {/*<PointHint*/}
                  {/*  name={name}*/}
                  {/*  isShown={selectedPoint.index === index}*/}
                  {/*/>*/}
                </View>
              </MapboxGL.PointAnnotation>
            ))}
          </MapView>
          <TextInput
            value={formData.title}
            onChangeText={e => handleFormInput(e, 'title')}
            style={styles.input}
            placeholder="Title"
            placeholderTextColor="#999"
          />
          {allFilters.map(({label, values, filter}, index) => (
            <View key={index} style={styles.filter}>
              <MyText style={styles.filter__title}>{label}</MyText>
              <ScrollView style={styles.filter__options} horizontal>
                {values.map((item, index) => (
                  <TouchableOpacity
                    onPress={() => checkFilterOption(filter, item)}
                    key={index}
                    activeOpacity={1}>
                    <Card
                      headerText={item.label}
                      headerIcon={item.icon}
                      headerStyle={{
                        color: item.color === '#f5f5f5' ? 'black' : `white`,
                        flexDirection: 'row',
                      }}
                      style={[
                        styles.filter__card,
                        formData[filter].find(x => x === item.value) && {
                          opacity: 1,
                        },
                        {backgroundColor: item.color},
                        index === 0 && {marginLeft: 15},
                        index === values.length - 1 && {marginRight: 15},
                      ]}
                      bodyStyle={styles.filter__card__body}
                    />
                  </TouchableOpacity>
                ))}
              </ScrollView>
            </View>
          ))}
          <TouchableOpacity
            onPress={publishTrip}
            style={styles.publishBtn}
            activeOpacity={0.8}>
            <MyText style={styles.publishBtn__text}>Publish this trip</MyText>
          </TouchableOpacity>
        </ScrollView>
      </KeyboardAvoidingView>
    </MyView>
  );
};
const mapStateToProps = state => ({});

export default connect(mapStateToProps, {sendMarkers, postTrip})(
  SummaryTripScreen,
);
