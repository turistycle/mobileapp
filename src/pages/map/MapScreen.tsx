import * as React from 'react';
import {useEffect, useState} from 'react';
import {connect} from 'react-redux';
import MyView from '../../components/MyView';
import MapView from '../../components/MapView';
import MapboxGL from '@react-native-mapbox-gl/maps';
import {TouchableOpacity, View} from 'react-native';
import Icon from '../../components/Icon';
import {getExternalPoints, getOptimizedRoute} from '../../actions/maps';
import {PointPopupInfo} from '../../components/MapComponents/PointPopupInfo';
import {DraggableList} from '../../components/MapComponents/DraggableList';
import {filterExternalPoints, pointInTwoLists} from '../../helpers/filters';

const styles = require('../../../assets/scss/pages/mapscreen.scss');

const MapScreen = ({route, navigation}) => {
  const [points, setPoints] = useState(route.params?.points || []);
  const [pointsFromBackend, setPointsFromBackend] = useState([]);
  const [firstCall, setFirstCall] = useState(true);
  const [draggableListShow, setDraggableListShow] = useState(false);
  const [selectedPoint, setSelectedPoint] = useState(null);
  const [centerUser, setCenterUser] = useState(true);
  const [geoJson, setGeoJson] = useState(null);

  useEffect(() => {
    if (firstCall) {
      getExternalPoints().then(setPointsFromBackend);
      setFirstCall(false);
    }
    if (route.params?.points) {
      addPoint(null, null).then(() => null);
    }
  }, [route.params]);

  const goNextStage = () => {
    navigation.navigate('SummaryTripScreen', {geoJson, points});
  };

  const removePoint = async index => {
    // points.filter((x, _index) => {
    //   console.log(x, _index, index);
    //   return _index !== index;
    // });
    const list = [
      ...points.filter((x, _index) => _index !== index).map(x => x.location),
    ];
    if (list.length) {
      const res = await getOptimizedRoute(list);
      shouldDeselectPoint();
      setGeoJson(res.trace);
      setPoints(res.waypoints);
    } else {
      shouldDeselectPoint();
      setGeoJson(null);
      setPoints([]);
    }
  };

  const addPoint = async (feature, existingPoint = null) => {
    if (!feature && !existingPoint) {
      const res = await getOptimizedRoute([
        ...route.params?.points.map(x => x.location),
      ]);
      setGeoJson(res.trace);
      setPoints(res.waypoints);
    } else {
      if (centerUser) setCenterUser(false);
      if (draggableListShow) {
        setDraggableListShow(false);
      } else if (selectedPoint !== null && existingPoint === null) {
        shouldDeselectPoint();
      } else if (points.length > 0) {
        const res = await getOptimizedRoute([
          ...points.map(x => x.location),
          existingPoint?.location || feature.geometry.coordinates,
        ]);
        if (existingPoint) {
          shouldDeselectPoint();
        }
        setGeoJson(res.trace);
        setPoints(res.waypoints);
      } else {
        setPoints([
          {
            waypoint_index: 0,
            location: existingPoint
              ? existingPoint.location
              : feature.geometry.coordinates,
          },
        ]);
        if (existingPoint) {
          shouldDeselectPoint();
        }
      }
    }
  };

  const changePointsOrder = async newList => {
    const res = await getOptimizedRoute([...newList.map(x => x.location)]);
    setGeoJson(res.trace);
    setPoints(res.waypoints);
  };

  const shouldDeselectPoint = (point = null) => {
    setSelectedPoint(point || null);
  };
  return (
    <MyView
      title={'Map'}
      withoutSafeArea
      withoutMargin
      style={styles.myview}
      withoutHeader
      withoutBackground>
      <PointPopupInfo
        isSelectedInPoints={
          !!points.find(
            x =>
              JSON.stringify(x.location) ===
              JSON.stringify(selectedPoint?.location),
          )
        }
        point={selectedPoint}
        onRemove={removePoint}
        onAddAsWaypoint={() => addPoint(null, selectedPoint)}
        onClose={() => shouldDeselectPoint()}
      />
      <DraggableList
        points={points}
        onChange={changePointsOrder}
        isShown={draggableListShow}
      />
      <MapView disableInteraction={false} onPress={addPoint}>
        <MapboxGL.Camera
          zoomLevel={13}
          // maxZoomLevel={13}
          followUserLocation={centerUser}
          centerCoordinate={selectedPoint?.location}
        />
        <MapboxGL.UserLocation visible />
        {geoJson && (
          <MapboxGL.ShapeSource id="myLine" shape={geoJson}>
            <MapboxGL.LineLayer id="myLineLayer" style={styles.map__line} />
            <MapboxGL.SymbolLayer
              minZoomLevel={1}
              id="symbolLocationSymbols"
              sourceID="symbolLocationSource"
              style={{
                iconImage: 'https://i.imgur.com/LcIng3L.png',
                iconAllowOverlap: true,
                symbolPlacement: 'line',
                symbolSpacing: 1,
                iconSize: 0.05,
                visibility: 'visible',
              }}
            />
          </MapboxGL.ShapeSource>
        )}
        {[...points, ...pointsFromBackend]
          .filter(filterExternalPoints)
          .map((point, index) => {
            const isInTwoLists = pointInTwoLists(
              points,
              pointsFromBackend,
              point,
            );
            let pointFromFirstList = null;
            if (isInTwoLists) {
              pointFromFirstList = points.find(
                x =>
                  JSON.stringify(x.location) === JSON.stringify(point.location),
              );
            }
            return (
              <MapboxGL.PointAnnotation
                key={index}
                id={`point${index}`}
                coordinate={point.location}
                selected={
                  selectedPoint?.waypoint_index ===
                  (point.waypoint_index ||
                    pointFromFirstList?.waypoint_index ||
                    index)
                }
                onDeselected={() => shouldDeselectPoint()}
                onSelected={() =>
                  shouldDeselectPoint(
                    isInTwoLists
                      ? {
                          ...point,
                          waypoint_index: pointFromFirstList.waypoint_index,
                        }
                      : point,
                  )
                }>
                {point._id && !isInTwoLists && (
                  <View style={styles.map__point} />
                )}
              </MapboxGL.PointAnnotation>
            );
          })}
      </MapView>
      <TouchableOpacity
        disabled={points.length <= 1}
        style={[
          styles.draggable__floatingButton,
          points.length <= 1 && {...styles.draggable__floatingButton__disabled},
        ]}
        onPress={() => setDraggableListShow(true)}>
        <Icon
          name={'drag'}
          size={30}
          style={styles.draggable__floatingButton__icon}
        />
      </TouchableOpacity>
      <TouchableOpacity
        disabled={points.length <= 1}
        style={[
          styles.create,
          points.length <= 1 && {...styles.create__disabled},
        ]}
        onPress={goNextStage}>
        {/*<View  style={{width:1, backgroundColor:'white', position:'absolute',height:'100%'}}/>*/}
        {/*<View  style={{height:1, backgroundColor:'white', position:'absolute',width:'100%'}}/>*/}
        <Icon name={'send'} size={30} style={styles.create__icon} />
      </TouchableOpacity>
    </MyView>
  );
};

export default connect()(MapScreen);
