import * as React from 'react';
import {useEffect, useRef, useState} from 'react';
import {
  ActivityIndicator,
  Animated,
  Dimensions,
  Easing,
  FlatList,
  TextInput,
  TouchableOpacity,
  View,
  ViewStyle,
} from 'react-native';
import {connect} from 'react-redux';
import MyView from '../../components/MyView';
import {getCommunityPosts} from '../../actions/community';
import MyText from '../../components/MyText';
import SocialPostItem from '../../components/SocialPostItem';
import Icon from '../../components/Icon';

const styles = require('../../../assets/scss/pages/socialscreen.scss');

const FOOTER: ViewStyle = {
  height: 20,
};

const FOOTER_LOADING: ViewStyle = {
  paddingBottom: 40,
  paddingTop: 20,
  width: '100%',
  alignSelf: 'center',
};

const SocialScreen = ({navigation, getCommunityPosts}) => {
  const [nextPage, setNextPage] = useState(1);
  const [isSearchMode, setIsSearchMode] = useState(false);
  const [extraData, setExtraData] = useState(1);
  const [isLoading, setIsLoading] = useState(false);
  const [refreshing, setRefreshing] = useState(true);
  const [posts, setPosts] = useState([]);
  const [searchingQuery, setSearchingQuery] = useState('');
  const animated = useRef(new Animated.Value(0)).current;
  const [inputRef, setInputRef] = useState(null);
  useEffect(() => {
    if (isSearchMode) {
      Animated.timing(animated, {
        toValue: Dimensions.get('window').width * 0.94,
        duration: 300,
        useNativeDriver: false,
        easing: Easing.linear,
      }).start();
    } else {
      Animated.timing(animated, {
        toValue: 0,
        duration: 300,
        useNativeDriver: false,
        easing: Easing.linear,
      }).start();
    }
  }, [isSearchMode]);

  const getFirstSet = async () => {
    setRefreshing(true);
    const response = await getCommunityPosts({});
    if (response) {
      console.log({response});
      setPosts(response.posts);
      setNextPage(response.nextPage);
      setExtraData(extraData + 1);
    }
    setRefreshing(false);
  };

  const getPaginatedData = async () => {
    if (nextPage) {
      setIsLoading(true);
      const response = await getCommunityPosts({
        nextPage,
        query: searchingQuery,
      });
      if (response) {
        setPosts([...posts, ...response.posts]);
        setNextPage(response.nextPage);
        setExtraData(extraData + 1);
      }
      setIsLoading(false);
    }
  };

  useEffect(() => {
    getFirstSet().then(() => null);
  }, []);
  return (
    <MyView
      withoutBackground
      withoutMargin
      title={isSearchMode ? '' : 'Community'}
      ButtonRight={({style}) =>
        isSearchMode ? (
          <View style={style.header__button}>
            <Animated.View
              style={[
                style.header__button,
                styles.search,
                {
                  flexDirection: 'row',
                  justifyContent: 'space-between',
                  width: animated,
                  right: '17%',
                  position: 'absolute',
                },
              ]}>
              <TextInput
                style={styles.search__text}
                value={searchingQuery}
                onChangeText={setSearchingQuery}
                placeholder="Search"
                placeholderTextColor="#555"
              />
              <TouchableOpacity
                style={{
                  aspectRatio: 1,
                  height: '100%',
                  alignItems: 'center',
                  justifyContent: 'center'
                }}
                onPress={() => setIsSearchMode(false)}
              ><MyText>x</MyText></TouchableOpacity>
            </Animated.View>
          </View>
        ) : (
          <TouchableOpacity
            onPress={() => setIsSearchMode(true)}
            style={style.header__button}>
            <Icon name="search" size={30} style={style.header__buttonIcon}/>
          </TouchableOpacity>
        )
      }>
      <FlatList
        style={{padding: "3%"}}
        showsVerticalScrollIndicator={false}
        data={posts.filter(x => x.displayName.match(searchingQuery))}
        extraData={extraData}
        onEndReachedThreshold={0}
        onEndReached={getPaginatedData}
        keyExtractor={(item, index) => `${index}`}
        refreshing={refreshing}
        onRefresh={getFirstSet}
        renderItem={({item, index}) => (
          <SocialPostItem key={index} item={item} navigation={navigation}/>
        )}
        ListEmptyComponent={
          !refreshing && (
            <MyText>None of your friends has posted anything yet...</MyText>
          )
        }
        ListFooterComponent={
          isLoading ? (
            <ActivityIndicator
              style={FOOTER_LOADING}
              size={'large'}
              color={'white'}
            />
          ) : (
            <View style={FOOTER}/>
          )
        }
      />
    </MyView>
  );
};
const mapStateToProps = state => ({});
export default connect(mapStateToProps, {getCommunityPosts})(SocialScreen);
