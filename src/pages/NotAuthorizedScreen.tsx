import * as React from 'react';
import {connect} from 'react-redux';
import MyView from '../components/MyView';
import {TouchableOpacity, View} from 'react-native';
import {resendVerificationEmail} from '../actions/auth';
import MyText from '../components/MyText';
import Card from '../components/Card';
import Icon from "../components/Icon";

const styles = require('../../assets/scss/pages/notauthorizedscreen.scss');

function NotAuthorizedScreen({user, navigation, resendVerificationEmail}) {
  return (
    <MyView
      title="Turistycle"
      ButtonRight={({style}) => (
        <TouchableOpacity
          onPress={() => navigation.replace('LoginNav')}
          style={style.header__button}>
          <Icon
            name="log-out"
            size={30}
            style={style.header__buttonIcon}
          />
        </TouchableOpacity>
      )}>
      <View style={styles.container}>
        <Card
          style={{maxHeight: null, marginBottom: 60}}
          headerText="Verify your email address">
          <MyText style={styles.textBlock}>
            Confirm your email address. Once it's done you will be able to start
            explore.
          </MyText>
          <MyText style={{...styles.textBlock}}>
            Haven't you received anything?
          </MyText>
          <TouchableOpacity
            style={{alignSelf: 'center'}}
            onPress={() => resendVerificationEmail(user)}>
            <MyText style={styles.link}>Click Here</MyText>
          </TouchableOpacity>
        </Card>
      </View>
    </MyView>
  );
}

const mapStateToProps = state => ({
  user: state.auth.user,
});

export default connect(mapStateToProps, {resendVerificationEmail})(
  NotAuthorizedScreen,
);
