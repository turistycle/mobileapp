import * as React from 'react';
import {useEffect, useState} from 'react';
import MyView from '../../../components/MyView';
import {connect} from 'react-redux';
import {
  ActivityIndicator,
  ScrollView,
  TouchableOpacity,
  View,
} from 'react-native';
import Icon from '../../../components/Icon';
import MyText from '../../../components/MyText';
import MapboxGL from '@react-native-mapbox-gl/maps';
import MapView from '../../../components/MapView';
import {getSingleTrip} from '../../../actions/search';
import {allFilters} from '../../../common/consts';
import Card from '../../../components/Card';

const styles = require('../../../../assets/scss/pages/tripdetailsscreen.scss');
const filterStyles = require('../../../../assets/scss/components/filtersmodal.scss');

const TripDetailsScreen = ({route, navigation, getSingleTrip}) => {
  const [isLoading, setIsLoading] = useState(true);
  const [item, setItem] = useState(route.params.item);
  const [selectedPoint, setSelectedPoint] = useState({index: null, zoom: null});
  const {bounds} = route.params;
  const [isDeselected, setIsDeselected] = useState(false);
  useEffect(() => {
    getSingleTrip(item._id).then(resp => {
      setItem(resp);
      console.log(resp);
      console.log(resp);
      setIsLoading(false);
    });
  }, []);

  const shouldDeselectPoint = (index = null, zoom = null) => {
    setSelectedPoint({index, zoom});
  };
  return (
    <MyView
      floatingHeader
      withoutBackground
      withoutMargin
      withoutSafeArea
      ButtonLeft={({style}) => (
        <TouchableOpacity
          onPress={() => navigation.pop()}
          style={{...style.header__button, transform: [{rotate: '90deg'}]}}>
          <Icon
            name={'chevron-down'}
            size={30}
            style={style.header__buttonIcon}
          />
        </TouchableOpacity>
      )}
      ButtonRight={({style}) => (
        <TouchableOpacity
          style={{...style.header__button}}
          onPress={() => {
            const points = item.markers.map(
              ({longitude, latitude, name}, index) => ({
                location: [longitude, latitude],
                name,
                waypoint_index: index,
              }),
            );
            navigation.navigate('MapNavigation', {points});
          }}>
          <Icon name={'map-pin'} size={25} style={style.header__buttonIcon} />
        </TouchableOpacity>
      )}
      title={'Trip'}>
      {isLoading ? (
        <ActivityIndicator size={30} color={'black'} />
      ) : (
        <ScrollView>
          <MapView style={styles.map} disableInteraction={false}>
            <MapboxGL.Camera
              bounds={selectedPoint.zoom ? null : {...bounds, paddingTop: 120}}
              animationDuration={800}
              animationMode={'flyTo'}
              centerCoordinate={selectedPoint.zoom}
            />
            <MapboxGL.ShapeSource id="line" shape={item.geoJson}>
              <MapboxGL.LineLayer id="lineLayer" style={styles.map__line} />
            </MapboxGL.ShapeSource>
            {item.markers.map(({longitude, latitude, name}, index) => (
              <MapboxGL.PointAnnotation
                onSelected={() =>
                  shouldDeselectPoint(index, [longitude, latitude])
                }
                onDeselected={() => shouldDeselectPoint()}
                snippet={'someSnippet'}
                title={name}
                key={name}
                id={name}
                coordinate={[longitude, latitude]}>
                <View style={styles.map__point}>
                  <PointHint
                    name={name}
                    isShown={selectedPoint.index === index}
                  />
                </View>
              </MapboxGL.PointAnnotation>
            ))}
          </MapView>
          <View style={styles.title_row}>
            <MyText style={styles.title_row__text}>{item.name}</MyText>
          </View>
          {allFilters
            .filter(x => item[x.filter].length > 0)
            .map(({label, values, filter}, index) => (
              <View key={index} style={filterStyles.filter}>
                <MyText style={filterStyles.filter__title}>{label}</MyText>
                <ScrollView style={filterStyles.filter__options} horizontal>
                  {values
                    .filter(x => item[filter].includes(x.value))
                    .map((item, index) => (
                      <View key={index}>
                        <Card
                          headerText={item.label}
                          headerIcon={item.icon}
                          headerStyle={{
                            color: item.color === '#f5f5f5' ? 'black' : `white`,
                            flexDirection: 'row',
                          }}
                          style={[
                            filterStyles.filter__card,
                            {opacity: 1},
                            {backgroundColor: item.color},
                            index === 0 && {marginLeft: 15},
                            index === values.length - 1 && {marginRight: 15},
                          ]}
                          bodyStyle={filterStyles.filter__card__body}
                        />
                      </View>
                    ))}
                </ScrollView>
              </View>
            ))}
        </ScrollView>
      )}
    </MyView>
  );
};

const PointHint = ({name, isShown = false}) => {
  return !isShown ? null : (
    <View
      style={{
        backgroundColor: 'rgba(0,0,0,0.40)',
        position: 'absolute',
        flex: 1,
        alignItems: 'center',
        bottom: '100%',
        marginBottom: 17,
        width: 200,
      }}>
      <MyText style={{color: 'white', fontSize: 20, flexDirection: 'row'}}>
        {name}
      </MyText>
      <View
        style={{
          position: 'absolute',
          top: '100%',
          width: 0,
          height: 0,
          backgroundColor: 'transparent',
          borderStyle: 'solid',
          borderLeftWidth: 10,
          borderRightWidth: 10,
          borderBottomWidth: 17,
          borderLeftColor: 'transparent',
          borderRightColor: 'transparent',
          borderBottomColor: `rgba(0,0,0,0.40)`,
          transform: [{rotate: '180deg'}],
        }}
      />
    </View>
  );
};

const mapStateToProps = state => ({});
export default connect(mapStateToProps, {getSingleTrip})(TripDetailsScreen);
