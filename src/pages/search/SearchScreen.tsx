import * as React from 'react';
import {useEffect, useState} from 'react';
import {connect} from 'react-redux';
import {ActivityIndicator, ScrollView, TextInput, TouchableOpacity, View,} from 'react-native';
import MyView from '../../components/MyView';
import Card from '../../components/Card';
import {findTrips} from '../../actions/search';
import TripCard from '../../components/SearchCards/TripCard';
import Icon from '../../components/Icon';
import MyText from '../../components/MyText';
import RBSheet from 'react-native-raw-bottom-sheet';
import FiltersModal from '../../components/FiltersModal';
import Badge from '../../components/Badge';
import {initialFiltersState} from "../../common/consts";

const styles = require('../../../assets/scss/pages/searchscreen.scss');
const filterTypes = [
  {
    id: 1,
    label: 'routes',
    value: 'routes',
  },
  // {
  //   id: 2,
  //   label: 'events',
  //   value: 'events',
  //   categories: [],
  // },
];


function SearchScreen({user, findTrips, navigation}) {
  const [isLoading, setIsLoading] = useState(false);
  const [isFirstCall, setIsFirstCall] = useState(true);
  const [query, setQuery] = useState('');
  const [currentFilters, setCurrentFilters] = useState(initialFiltersState);
  const [currentFilterType, setCurrentFilterType] = useState(filterTypes[0]);
  const [RBSheetRef, setRBSheetRef] = useState(null);
  const [results, setResults] = useState(null);

  useEffect(() => {
    if (isFirstCall) {
      setIsFirstCall(false);
    } else {
      console.log(currentFilters);
      setIsLoading(true);
      makeSearch().then(() => {
        setIsLoading(false);
      });
    }
  }, [currentFilters]);

  const makeSearch = async () => {
    let data = [];
    if (currentFilterType.id === 1) {
      data = await findTrips({
        ...currentFilters,
        query: query || null,
      });
    }
    setResults(data);
  };

  const changeFilterType = filter => {
    setCurrentFilterType(filter);
  };

  const badgeValue = () => {
    let counter = 0;

    for (const key in currentFilters) {
      if (
        !['distanceMin', 'distanceMax'].includes(key) &&
        currentFilters[key].length > 0
      ) {
        counter++;
      }
    }
    if (currentFilters.distanceMin > 1 || currentFilters.distanceMax < 250) {
      counter++;
    }

    return counter;
  };

  const navigateToDetails = (item, bounds) => {
    navigation.navigate('TripDetailsScreen', {item, bounds});
  };

  return (
    <MyView title="Search" withoutHeader withoutMargin>
      <View style={styles.searchBar}>
        <TextInput
          style={styles.searchBar__text}
          placeholder="Search"
          value={query}
          onChangeText={setQuery}
          placeholderTextColor="#ccc"
        />
        <View style={styles.searchBar__buttons}>
          <TouchableOpacity
            onPress={() => RBSheetRef.open()}
            style={[
              styles.searchBar__button,
              styles['searchBar__button--filter'],
            ]}>
            <Icon
              size={20}
              name="sliders"
              style={styles['searchBar__button--filter__icon']}
            />
            {badgeValue() > 0 && <Badge value={badgeValue()}/>}
          </TouchableOpacity>
          <TouchableOpacity
            style={[
              styles.searchBar__button,
              query.trim() === '' && styles['searchBar__button--disabled'],
            ]}
            disabled={query.trim() === ''}
            onPress={makeSearch}>
            <Icon
              size={20}
              name="search"
              style={styles.searchBar__button__icon}
            />
          </TouchableOpacity>
        </View>
      </View>
      {/*<View style={styles.topTypes}>*/}
      {/*  {filterTypes.map((filter, index) => (*/}
      {/*    <TouchableOpacity*/}
      {/*      onPress={() => changeFilterType(filter)}*/}
      {/*      key={index}*/}
      {/*      activeOpacity={1}>*/}
      {/*      <Card*/}
      {/*        headerText={filter.label}*/}
      {/*        style={[*/}
      {/*          styles.topTypes__card,*/}
      {/*          currentFilterType.id === filter.id &&*/}
      {/*          styles['topTypes__card--active'],*/}
      {/*        ]}*/}
      {/*        bodyStyle={styles.topTypes__card__body}*/}
      {/*      />*/}
      {/*    </TouchableOpacity>*/}
      {/*  ))}*/}
      {/*</View>*/}
      <ScrollView style={styles.results}>
        {isLoading ? (
          <ActivityIndicator size={20}/>
        ) : results === null ? (
          <MyText>Nothing to show</MyText>
        ) : (
          results.map((item, index) => (
            <TripCard
              navigateToDetails={navigateToDetails}
              item={item}
              key={index}
            />
          ))
        )}
      </ScrollView>
      <RBSheet
        ref={setRBSheetRef}
        openDuration={250}
        height={490}
        closeOnDragDown={true}
        customStyles={{
          container: {
            justifyContent: 'center',
            alignItems: 'center',
            backgroundColor: 'transparent',
            paddingBottom: 50,
          },
        }}>
        <FiltersModal
          clearFilters={() => {
            setCurrentFilters(initialFiltersState)
            RBSheetRef.close();
          }}
          updateFilters={val => {
            setCurrentFilters(val);
            RBSheetRef.close();
          }}
          currentFilters={currentFilters}
        />
      </RBSheet>
    </MyView>
  );
}

const mapStateToProps = state => ({
  user: state.auth.user,
});

export default connect(mapStateToProps, {findTrips})(SearchScreen);
