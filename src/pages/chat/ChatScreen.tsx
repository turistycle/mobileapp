import * as React from 'react';
import {useEffect, useState} from 'react';
import {ScrollView, TextInput, TouchableOpacity, View} from 'react-native';
import MyView from '../../components/MyView';
import {connect} from 'react-redux';
import Icon from '../../components/Icon';
import ConversationItem from '../../components/ConversationItem';
import {useSafeAreaInsets} from 'react-native-safe-area-context';
import NewConversationSearch from '../../components/NewConversationSearch';
import {resetNotification} from '../../actions/notifications';
import MyText from "../../components/MyText";

const styles = require('../../../assets/scss/pages/chatscreen.scss');

const ChatScreen = ({user, navigation, conversations, resetNotification}) => {
  const insets = useSafeAreaInsets();
  const [searchUser, setSearchUser] = useState('');
  const [newConversation, setNewConversation] = useState(false);
  useEffect(() => {
    resetNotification();
    return () => resetNotification();
  }, []);
  return newConversation ? (
    <NewConversationSearch
      onDismiss={() => setNewConversation(false)}
      navigation={navigation}
    />
  ) : (
    <MyView
      withoutBackground
      title="Chat"
      ButtonLeft={({style}) => (
        <TouchableOpacity
          onPress={() => navigation.pop()}
          style={{...style.header__button, transform: [{rotate: '90deg'}]}}>
          <Icon
            name={'chevron-down'}
            size={30}
            style={style.header__buttonIcon}
          />
        </TouchableOpacity>
      )}>
      <ScrollView>
        <View style={styles.search}>
          <TextInput
            onChangeText={setSearchUser}
            value={searchUser}
            style={styles.search__input}
            placeholder="Search conversation..."
            placeholderTextColor="#999"
          />
          {searchUser !== '' && (
            <TouchableOpacity>
              <Icon style={styles.search__dismiss} size={15} name="gears"/>
            </TouchableOpacity>
          )}
        </View>
        {conversations
          .filter(x =>
            x.users
              .find(y => y.uid !== user.uid)
              .displayName.toLowerCase()
              .match(searchUser.toLowerCase()),
          )
          .sort(
            (a, b) =>
              b.lastMessage.time.seconds -
              a.lastMessage.time.seconds,
          )
          .map((conversation, index) => (
            <ConversationItem
              conversation={conversation}
              key={index}
              onPress={secondUser =>
                navigation.navigate('SingleConversationScreen', {
                  conversationId: conversation.id,
                  secondUser,
                })
              }
            />
          ))}
      </ScrollView>
      <TouchableOpacity
        onPress={() => setNewConversation(true)}
        style={{...styles.floating, bottom: insets.bottom !== 0 ? 20 : 20}}>
        <Icon name={'create'} style={styles.floating__icon} size={20}/>
      </TouchableOpacity>
    </MyView>
  );
};

const mapStateToProps = state => ({
  conversations: state.chat.conversations,
  user: state.auth.user,
});
export default connect(mapStateToProps, {resetNotification})(ChatScreen);
