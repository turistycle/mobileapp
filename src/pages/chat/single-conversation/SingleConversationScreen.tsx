import * as React from 'react';
import {useEffect, useState} from 'react';
import MyView from '../../../components/MyView';
import {connect} from 'react-redux';
import {
  ActivityIndicator,
  Image,
  KeyboardAvoidingView,
  Platform,
  TextInput,
  TouchableOpacity,
  View,
} from 'react-native';
import InvertibleScrollView from 'react-native-invertible-scroll-view';
import MyText from '../../../components/MyText';
import Icon from '../../../components/Icon';
import {resetCurrentChat, sendFirstMessage, sendMessage, subscribeCurrentChat,} from '../../../actions/chat';
import {firebase} from '@react-native-firebase/firestore';
import {messageDate} from '../../../helpers/dates';

const styles = require('../../../../assets/scss/pages/singleconversationscreen.scss');
const mainpageStyles = require('../../../../assets/scss/pages/mainpage');

const Avatar = require('../../../../assets/images/avatar.png');

const SingleConversationScreen = ({
                                    navigation,
                                    route,
                                    user,
                                    messages,
                                    subscribeCurrentChat,
                                    resetCurrentChat,
                                    sendFirstMessage,
                                  }) => {
  const [conversationId, setConversationId] = useState(
    route.params.conversationId,
  );
  const [isLoading, setIsLoading] = useState(true);
  const {secondUser} = route.params;
  const [scrollView, setScrollView] = useState(null);
  const [messageText, setMessageText] = useState('');

  useEffect(() => {
    if (conversationId) {
      subscribeCurrentChat(conversationId).then(() => {
        setIsLoading(false);
      });
    } else {
      setIsLoading(false);
    }
    return () => resetCurrentChat();
  }, [conversationId]);

  const sendAsFirstMessage = async () => {
    setMessageText('');
    await sendFirstMessage(
      secondUser,
      {
        text: messageText,
        time: firebase.firestore.Timestamp.fromDate(new Date()),
        senderUid: user.uid,
      },
      setConversationId,
    );
  };

  const send = async () => {
    setMessageText('');
    await sendMessage(conversationId, {
      text: messageText,
      time: firebase.firestore.Timestamp.fromDate(new Date()),
      senderUid: user.uid,
    });
  };

  return (
    <KeyboardAvoidingView
      behavior={Platform.OS === 'ios' ? 'padding' : 'height'}
      style={styles.flex1}>
      <MyView
        titleStyle={styles.title}
        withoutBackground
        title={secondUser.displayName}
        ButtonLeft={({style}) => (
          <TouchableOpacity
            onPress={() => navigation.pop()}
            style={{...style.header__button, transform: [{rotate: '90deg'}]}}>
            <Icon name={'chevron-down'} size={30} color={'#171717'}/>
          </TouchableOpacity>
        )}
        ButtonRight={({style}) => (
          <TouchableOpacity style={style.header__button} onPress={() => null}>
            <Image
              style={mainpageStyles.imageCircle}
              source={
                !secondUser.photoURL ? Avatar : {uri: secondUser.photoURL}
              }
            />
          </TouchableOpacity>
        )}>
        {isLoading ? (
          <ActivityIndicator size={20} color={'black'}/>
        ) : (
          <View style={{flex: 1}}>
            <InvertibleScrollView
              inverted={true}
              showsVerticalScrollIndicator={false}
              showsHorizontalScrollIndicator={false}
              ref={ref => {
                setScrollView(ref);
              }}
              // contentOffset={{y}}
              onContentSizeChange={() => {
                scrollView.scrollTo({y: 0, animated: true});
              }}
              style={styles.container}>
              {conversationId === null && messages.length === 0 ? (
                <MyText style={styles.firstMessage}>
                  Write your first message
                </MyText>
              ) : (
                messages.map((message, index) => {
                  let date;
                  try {
                    date = new Date(message.time).toUTCString();
                  } catch {
                    date = message.time;
                  }
                  return (
                    <View key={index}>
                      {message.isDateShow && (
                        <View style={styles.time}>
                          <View style={styles.time__line}/>
                          <MyText style={styles.time__value}>
                            {messageDate(date)}
                          </MyText>
                          <View style={styles.time__line}/>
                        </View>
                      )}
                      <View
                        style={
                          message.senderUid !== secondUser.uid
                            ? styles.messageRight
                            : styles.messageLeft
                        }>
                        <MyText>{message.text}</MyText>
                      </View>
                    </View>
                  );
                })
              )}
            </InvertibleScrollView>
            <View style={styles.chat__inputField}>
              <TextInput
                onFocus={() => scrollView.scrollTo({y: 0, animated: true})}
                style={styles.chat__inputFieldText}
                value={messageText}
                onChangeText={setMessageText}
                placeholder="Message"
                placeholderTextColor="#555"
              />
              <TouchableOpacity
                onPress={conversationId ? send : sendAsFirstMessage}
                style={styles.chat__button}
                disabled={messageText === null || messageText.trim() === ''}>
                <Icon
                  style={{
                    ...styles.chat__buttonSendIcon,
                    opacity:
                      messageText === null || messageText.trim() === ''
                        ? 0.5
                        : 1,
                  }}
                  size={20}
                  name={'send'}
                />
              </TouchableOpacity>
            </View>
          </View>
        )}
      </MyView>
    </KeyboardAvoidingView>
  );
};

const mapStateToProps = state => ({
  messages: state.chat.currentConversation.messages,
  user: state.auth.user,
});

export default connect(mapStateToProps, {
  subscribeCurrentChat,
  resetCurrentChat,
  sendFirstMessage,
})(SingleConversationScreen);
