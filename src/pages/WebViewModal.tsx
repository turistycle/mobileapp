import * as React from 'react';
import {View} from 'react-native';
import {WebView} from 'react-native-webview';

const WebViewModal = (props) => {
  return (
    <View
      style={{
        borderTopLeftRadius: 12,
        borderTopRightRadius: 12,
        backgroundColor: 'white',
        flex: 1,
      }}>
      <View
        style={{
          marginVertical: 10,
          marginHorizontal: '30%',
          height: 5,
          backgroundColor: '#494949',
          borderRadius: 5,
        }}
      />
      <WebView source={{uri: props.route.params.uri}}/>
    </View>
  );
};

export default WebViewModal;
