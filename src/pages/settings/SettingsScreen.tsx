import * as React from 'react';
import {connect} from 'react-redux';
import MyView from '../../components/MyView';
import LongButton, {LongButtonProps} from '../../components/LongButton';
import {Image, ScrollView, View} from 'react-native';
import MyText from '../../components/MyText';
import {logOut} from '../../actions/auth';

const styles = require('../../../assets/scss/pages/settingsscreen.scss');

interface buttonProps {
  title: string;
  icon: string;
  onPress: () => string;
  style?: any;
}

const SettingsScreen = ({navigation, user, logOut}) => {
  const buttons: buttonProps[] = [
    {
      title: 'Account Details',
      icon: 'chevron-down',
      style: {transform: [{rotate: '270deg'}]},
      onPress: () => navigation.navigate('UserScreen'),
    },
    {
      title: 'Messages',
      icon: 'chevron-down',
      style: {transform: [{rotate: '270deg'}]},
      onPress: () => navigation.navigate('ChatNav'),
    },
  ];
  const restButtons: buttonProps[] = [
    {
      title: 'Terms & Conditions',
      icon: 'chevron-down',
      style: {transform: [{rotate: '270deg'}]},
      onPress: () =>
        navigation.push('WebViewModal', {
          uri: 'https://turistycle.web.app',
        }),
    },
    {
      title: 'About',
      icon: 'chevron-down',
      style: {transform: [{rotate: '270deg'}]},
      onPress: () =>
        navigation.push('WebViewModal', {
          uri: 'https://turistycle.web.app',
        }),
    },
    {
      title: 'Need Help?',
      icon: 'chevron-down',
      style: {transform: [{rotate: '270deg'}]},
      onPress: () =>
        navigation.push('WebViewModal', {
          uri: 'https://turistycle.web.app',
        }),
    },
    {
      title: 'Log Out',
      icon: 'log-out',
      onPress: () => logOut(() => navigation.replace('LoginNav')),
    },
  ];
  return (
    <MyView title="Menu" withoutHeader={true}>
      <ScrollView>
        <View style={styles.user}>
          <View style={styles.user__image}>
            <Image
              source={{uri: user.photoURL}}
              style={styles.user__imageImage}
            />
          </View>
          <MyText style={styles.user__name}>
            Hello,{' '}
            <MyText style={[styles.user__name, {fontWeight: '700'}]}>
              {user.displayName}
            </MyText>
            !
          </MyText>
        </View>
        <View style={styles.buttons}>
          {buttons.map(
            ({title, icon, onPress, style}: LongButtonProps, index) => (
              <LongButton
                style={style}
                key={index}
                title={title}
                icon={icon}
                onPress={onPress}
              />
            ),
          )}
        </View>
        <View style={styles.buttons}>
          {restButtons.map(
            ({title, icon, onPress, style}: LongButtonProps, index) => (
              <LongButton
                style={style}
                key={index}
                title={title}
                icon={icon}
                onPress={onPress}
              />
            ),
          )}
        </View>
      </ScrollView>
    </MyView>
  );
};

const mapStateToProps = state => ({
  user: state.auth.user,
});
export default connect(mapStateToProps, {logOut})(SettingsScreen);
