import * as React from 'react';
import {useState} from 'react';
import {connect} from 'react-redux';
import MyView from '../../../components/MyView';
import {Image, SafeAreaView, ScrollView, TouchableOpacity, View,} from 'react-native';
import MyText from '../../../components/MyText';
import Icon from '../../../components/Icon';
import {RNCamera} from 'react-native-camera';
import {launchImageLibrary} from 'react-native-image-picker';

const styles = require('../../../../assets/scss/pages/userscreen.scss');

const UserScreen = ({navigation, user}) => {
  const [cameraType, setCameraType] = useState(RNCamera.Constants.Type.front);
  const [cameraOn, setCameraOn] = useState(false);
  const [camera, setCamera] = useState(null);

  const capturePhoto = async () => {
    const photo = await camera.takePictureAsync({quality: 0.5});
    setCameraOn(false);
    // updateProfilePhoto(
    //   user,
    //   Platform.OS === 'ios' ? photo.uri.replace('file://', '') : photo.uri,
    // );
  };
  const captureFromLibrary = () => {
    launchImageLibrary(
      {
        mediaType: 'photo',
        quality: 0.5,
      },
      response => {
        if (!response.didCancel) {
          // updateProfilePhoto(
          //   user,
          //   Platform.OS === 'ios'
          //     ? response.assets[0].uri.replace('file://', '')
          //     : response.assets[0].uri,
          // );
          setCameraOn(false);
        }
      },
    );
  };

  return cameraOn ? (
    <SafeAreaView style={styles.camera}>
      <View style={styles.camera__block}>
        <RNCamera
          autoFocus="on"
          maxZoom={2}
          ratio="1:1"
          style={styles.camera__blockView}
          ref={setCamera}
          captureAudio={false}
          type={cameraType}
          flashMode={RNCamera.Constants.FlashMode.auto}
          androidCameraPermissionOptions={{
            title: 'Permission to use camera',
            message: 'We need your permission to use your camera',
            buttonPositive: 'Ok',
            buttonNegative: 'Cancel',
          }}
        />
        {/*Button dismiss camera*/}
        <TouchableOpacity
          onPress={() => setCameraOn(false)}
          style={styles.camera__dismiss}>
          <Icon color="white" size={28} name="x"/>
        </TouchableOpacity>

        {/*Button capture image*/}
        <TouchableOpacity onPress={capturePhoto} style={styles.camera__capture}>
        </TouchableOpacity>

        {/*Button select from library*/}
        <TouchableOpacity
          onPress={captureFromLibrary}
          style={styles.camera__library}>
          <Icon color="white" size={30} name="image"/>
        </TouchableOpacity>

        {/*Button swap camera type*/}
        <TouchableOpacity
          onPress={() =>
            setCameraType(
              cameraType === RNCamera.Constants.Type.back
                ? RNCamera.Constants.Type.front
                : RNCamera.Constants.Type.back,
            )
          }
          style={styles.camera__swap}>
          <Icon color="white" size={28} name="repeat"/>
        </TouchableOpacity>
      </View>
    </SafeAreaView>
  ) : (
    <MyView
      title="Account"
      ButtonLeft={({style}) => (
        <TouchableOpacity
          onPress={() => navigation.pop()}
          style={{...style.header__button, transform: [{rotate: '90deg'}]}}>
          <Icon
            name={'chevron-down'}
            size={30}
            style={style.header__buttonIcon}
          />
        </TouchableOpacity>
      )}>
      <ScrollView contentContainerStyle={styles.user}>
        <View style={styles.user__image}>
          <Image
            source={{uri: user.photoURL}}
            style={styles.user__imageImage}
          />
          <TouchableOpacity
            activeOpacity={0.7}
            onPress={() => setCameraOn(true)}
            style={styles.user__imageButton}>
            <Icon
              style={styles['user__imageButton--icon']}
              size={20}
              name="edit"
            />
          </TouchableOpacity>
        </View>
        <MyText>{user.displayName}</MyText>
      </ScrollView>
    </MyView>
  );
};

const mapStateToProps = state => ({
  user: state.auth.user,
});
export default connect(mapStateToProps)(UserScreen);
