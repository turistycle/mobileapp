import * as React from 'react';
import {useEffect, useState} from 'react';
import {ImageBackground, SafeAreaView, ScrollView, Switch, TextInput, TouchableOpacity, View,} from 'react-native';
import {connect} from 'react-redux';
import {signUpWithEmail} from '../actions/auth';
import MyText from '../components/MyText';
import Header from '../components/Header';
import Icon from "../components/Icon";

const styles = require('../../assets/scss/pages/loginpage.scss');

const logo = require('../../assets/images/bg_no_bikes.png');


const RegisterScreen = ({navigation, signUpWithEmail, user}) => {
  const [isFocus, setIsFocus] = useState({
    login: false,
    password: false,
    repeatPassword: false,
    displayName: false,
  });
  const [formData, setFormData] = useState({
    login: 'mczarczynski@edu.cdv.pl',
    password: 'Kaczuszka123',
    repeatPassword: 'Kaczuszka123',
    displayName: 'WiktorG',
    termsAgree: true,
  });

  const [isFirstCall, setIsFirstCall] = useState(true);

  useEffect(() => {
    console.log({styles})
    if (!isFirstCall && user?.uid) {
      if (
        !user?.emailVerified &&
        user?.providerData?.[0].providerId !== 'facebook.com'
      ) {
        navigation.replace('NotAuthorized');
      } else {
        navigation.replace('MainTab');
      }
    } else if (isFirstCall) {
      setIsFirstCall(false);
    }
  }, [user]);

  const handleFormInput = (e, field) => {
    switch (field) {
      case 'login':
        setFormData({...formData, login: e});
        break;
      case 'displayName':
        setFormData({...formData, displayName: e});
        break;
      case 'password':
        setFormData({...formData, password: e});
        break;
      case 'repeatPassword':
        setFormData({...formData, repeatPassword: e});
        break;
      case 'termsAgree':
        setFormData({...formData, termsAgree: e});
        break;
      default:
        return;
    }
  };
  return (
    <ImageBackground
      source={logo}
      style={styles.background}
      imageStyle={styles['background__image--sign-up']}>
      <Header darkText title="Sign Up"/>
      <SafeAreaView style={styles['sign-up']}>
        <ScrollView contentContainerStyle={styles.container}>
          <View style={styles.inputs}>
            <View
              style={{
                ...styles.input_around,
                ...(isFocus.login ? styles.input_around_focus : []),
              }}>
              <Icon name="user"/>
              <TextInput
                value={formData.login}
                autoCompleteType="email"
                onChangeText={e => handleFormInput(e, 'login')}
                onFocus={() => setIsFocus({...isFocus, login: true})}
                onBlur={() => setIsFocus({...isFocus, login: false})}
                style={styles.input_around__text}
                placeholder="email"
                placeholderTextColor="#555"
              />
            </View>
            <View
              style={{
                ...styles.input_around,
                ...(isFocus.displayName ? styles.input_around_focus : []),
              }}>
              <Icon name="user"/>
              <TextInput
                value={formData.displayName}
                // autoCompleteType="email"
                onChangeText={e => handleFormInput(e, 'displayName')}
                onFocus={() => setIsFocus({...isFocus, displayName: true})}
                onBlur={() => setIsFocus({...isFocus, displayName: false})}
                style={styles.input_around__text}
                placeholder="username"
                placeholderTextColor="#555"
              />
            </View>
            <View
              style={{
                ...styles.input_around,
                ...(isFocus.password ? styles.input_around_focus : []),
              }}>
              <Icon name="user"/>
              <TextInput
                value={formData.password}
                autoCompleteType="password"
                onChangeText={e => handleFormInput(e, 'password')}
                textContentType="password"
                secureTextEntry={true}
                onFocus={() => setIsFocus({...isFocus, password: true})}
                onBlur={() => setIsFocus({...isFocus, password: false})}
                style={styles.input_around__text}
                placeholder="password"
                placeholderTextColor="#555"
              />
            </View>
            <View
              style={{
                ...styles.input_around,
                ...(isFocus.repeatPassword ? styles.input_around_focus : []),
              }}>
              <Icon name="user"/>
              <TextInput
                value={formData.repeatPassword}
                autoCompleteType="password"
                onChangeText={e => handleFormInput(e, 'repeatPassword')}
                textContentType="password"
                secureTextEntry={true}
                onFocus={() => setIsFocus({...isFocus, repeatPassword: true})}
                onBlur={() => setIsFocus({...isFocus, repeatPassword: false})}
                style={styles.input_around__text}
                placeholder="repeat password"
                placeholderTextColor="#555"
              />
            </View>
          </View>
          <View
            style={{
              flexDirection: 'row',
              alignItems: 'center',
              width: '100%',
            }}>
            <Switch
              style={{marginRight: 20}}
              trackColor={styles.switch_colors}

              // ios_backgroundColor="#3e3e3e"
              onValueChange={e => handleFormInput(e, 'termsAgree')}
              value={formData.termsAgree}
            />
            <MyText>I agree to the </MyText>
            <TouchableOpacity
              onPress={() =>
                navigation.push('WebViewModal', {
                  uri: 'https://turistycle.web.app',
                })
              }>
              <MyText style={styles.terms_text}>Terms and Conditions</MyText>
            </TouchableOpacity>
          </View>
          <TouchableOpacity
            onPress={() =>
              signUpWithEmail(
                formData.login,
                formData.displayName,
                formData.password,
                formData.repeatPassword,
                formData.termsAgree,
              )
            }
            style={{...styles.button, ...styles.button__sign_in}}>
            <MyText style={styles.button__sign_in__text}>Sign Up</MyText>
          </TouchableOpacity>
          <TouchableOpacity
            onPress={() => navigation.replace('Login')}
            style={{...styles.button, ...styles.button__sign_up}}>
            <MyText style={styles.button__sign_up__text}>
              already have an account?
            </MyText>
          </TouchableOpacity>
        </ScrollView>
      </SafeAreaView>
    </ImageBackground>
  );
};

const mapStateToProps = state => ({
  user: state.auth.user,
});

export default connect(mapStateToProps, {signUpWithEmail})(RegisterScreen);
