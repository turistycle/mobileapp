import * as React from 'react';
import {useEffect, useState} from 'react';
import {
  ImageBackground,
  SafeAreaView,
  ScrollView,
  TextInput,
  TouchableOpacity,
  View,
} from 'react-native';
import {connect} from 'react-redux';
import {
  signInWithEmail,
  signInWithFacebook,
  signInWithGoogle,
} from '../actions/auth';
import MyText from '../components/MyText';
import Header from '../components/Header';
import GoogleSvg from '../../assets/images/GoogleSvg';
import FacebookSvg from '../../assets/images/FacebookSVG';
import Icon from '../components/Icon';

const styles = require('../../assets/scss/pages/loginpage.scss');

const logo = require('../../assets/images/bg_no_bikes.png');

const LoginScreen = ({
  navigation,
  signInWithEmail,
  signInWithGoogle,
  signInWithFacebook,
  user,
}) => {
  const [isFocus, setIsFocus] = useState({login: false, password: false});
  const [isFirstCall, setIsFirstCall] = useState(true);
  const [formData, setFormData] = useState({
    login: 'mczarczynski@edu.cdv.pl',
    password: 'Kaczuszka123',
  });

  useEffect(() => {
    if (!isFirstCall && user.uid) {
      if (
        !user?.emailVerified &&
        user?.providerData?.[0].providerId !== 'facebook.com'
      ) {
        navigation.replace('NotAuthorized');
      } else {
        navigation.replace('MainTab');
      }
    } else if (isFirstCall) {
      setIsFirstCall(false);
    }
  }, [user]);

  const handleFormInput = (e, field) => {
    if (field === 'login') setFormData({...formData, login: e});
    else if (field === 'password') setFormData({...formData, password: e});
  };
  return (
    <ImageBackground
      source={logo}
      style={styles.background}
      imageStyle={styles.background__image}>
      <Header darkText title="Sign In" />
      <SafeAreaView style={styles['sign-in']}>
        <ScrollView contentContainerStyle={styles.container}>
          <View style={styles.inputs}>
            <View
              style={{
                ...styles.input_around,
                ...(isFocus.login ? styles.input_around_focus : []),
              }}>
              <Icon name="user" style={{marginLeft: 20}} />
              <TextInput
                value={formData.login}
                autoCompleteType="email"
                onChangeText={e => handleFormInput(e, 'login')}
                onFocus={() => setIsFocus({...isFocus, login: true})}
                onBlur={() => setIsFocus({...isFocus, login: false})}
                style={styles.input_around__text}
                placeholder="email"
                placeholderTextColor="#555"
              />
            </View>
            <View
              style={{
                ...styles.input_around,
                ...(isFocus.password ? styles.input_around_focus : []),
              }}>
              <Icon name="user" style={{marginLeft: 20}} />
              <TextInput
                value={formData.password}
                autoCompleteType="password"
                onChangeText={e => handleFormInput(e, 'password')}
                textContentType="password"
                secureTextEntry={true}
                onFocus={() => setIsFocus({...isFocus, password: true})}
                onBlur={() => setIsFocus({...isFocus, password: false})}
                style={styles.input_around__text}
                placeholder="password"
                placeholderTextColor="#555"
              />
            </View>
            <MyText style={{textAlign: 'center'}}>
              forgot password?{' '}
              <MyText
                style={{color: styles.switch_colors.true}}
                onPress={() => console.log('lalla')}>
                Click here
              </MyText>
            </MyText>
          </View>

          {/*BUTTONS*/}
          <View style={styles.container__buttons}>
            <TouchableOpacity
              onPress={() => signInWithEmail(formData.login, formData.password)}
              style={{...styles.button, ...styles.button__sign_in}}>
              <MyText style={styles.button__sign_in__text}>Sign In</MyText>
            </TouchableOpacity>
            <View style={styles['or']}>
              <MyText style={styles['or__text']}>or login with</MyText>
            </View>
            <View style={styles['buttons-group']}>
              <TouchableOpacity
                onPress={signInWithGoogle}
                style={styles['buttons-group__button']}>
                <GoogleSvg style={styles['buttons-group__button__icon']} />
              </TouchableOpacity>
              <TouchableOpacity
                onPress={signInWithFacebook}
                style={styles['buttons-group__button']}>
                <FacebookSvg style={styles['buttons-group__button__icon']} />
              </TouchableOpacity>
            </View>
          </View>
          <TouchableOpacity
            onPress={() => navigation.replace('Register')}
            style={{...styles.button, ...styles.button__sign_up}}>
            <MyText style={styles.button__sign_up__text}>create account</MyText>
          </TouchableOpacity>
        </ScrollView>
      </SafeAreaView>
    </ImageBackground>
  );
};

const mapStateToProps = state => ({
  user: state.auth.user,
});

export default connect(mapStateToProps, {
  signInWithEmail,
  signInWithGoogle,
  signInWithFacebook,
})(LoginScreen);
