import * as React from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import SearchScreen from '../pages/search/SearchScreen';
import TripDetailsScreen from "../pages/search/trip-details-screen/TripDetailsScreen";

const {Navigator, Screen} = createStackNavigator();

export default () => (
  <Navigator initialRouteName="SearchScreen">
    <Screen
      name="SearchScreen"
      component={SearchScreen}
      options={{
        header: () => null,
      }}
    />
    <Screen
      name="TripDetailsScreen"
      options={{
        header: () => null,
      }}>
      {params => <TripDetailsScreen {...params} />}
    </Screen>
  </Navigator>
);
