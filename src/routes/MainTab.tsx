import * as React from 'react';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import MainPage from '../pages/MainScreen';
import SettingsNavigation from './SettingsNavigation';
import BottomTabSlider from '../components/BottomTabSlider';
import MapNavigation from './MapNavigation';
import SocialNavigation from './SocialNavigation';
import SearchNavigation from './SearchNavigation';

const {tabBarColor} = require('../../assets/scss/settings/colors.scss');
const {Navigator, Screen} = createBottomTabNavigator();

export default function () {
  return (
    <Navigator
      initialRouteName="DashboardScreen"
      screenOptions={({route}) => ({
        tabBarIcon: props => {
          return <BottomTabSlider route={route} {...props} />;
        },
      })}
      tabBarOptions={{
        showLabel: false,
        style: {
          // height: 50,
          // paddingBottom: 10,
          borderTopWidth: 0,
          backgroundColor: tabBarColor.color,
        },
        activeTintColor: 'whitesmoke',
        inactiveTintColor: 'whitesmoke',
      }}>
      <Screen
        name="DashboardScreen"
        component={MainPage}
        options={{title: 'Main page'}}
      />
      <Screen
        name="SearchNavigation"
        component={SearchNavigation}
        options={{
          title: 'Search navigation',
        }}
      />
      <Screen
        name="MapNavigation"
        options={{
          title: 'Map navigation',
        }}>
        {params => <MapNavigation {...params} />}
      </Screen>
      <Screen
        name="SocialNavigation"
        component={SocialNavigation}
        options={{
          title: 'Social navigation',
        }}
      />
      <Screen name="SettingsNavigation" component={SettingsNavigation} />
    </Navigator>
  );
}
