import * as React from 'react';
import {useEffect} from 'react';
import 'react-native-gesture-handler';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator, TransitionPresets} from '@react-navigation/stack';
import {AppearanceProvider} from 'react-native-appearance';
import {connect} from 'react-redux';
import MainTab from './MainTab';
import ChatNavigation from './ChatNavigation';
import LoginScreen from '../pages/LoginScreen';
import RegisterScreen from '../pages/RegisterScreen';
import WebViewModal from '../pages/WebViewModal';
import NotAuthorizedScreen from '../pages/NotAuthorizedScreen';
import {subscribeChats, unsubscribeChats} from '../actions/chat';

const NAV_MODAL_CARD_STYLE = {
  borderTopLeftRadius: 20,
  borderTopRightRadius: 20,
  backgroundColor: '#00000020',
  marginTop: '20%',
};
const MODAL_STACK_OPTIONS = {
  headerShown: false,
  cardStyle: {
    backgroundColor: '#00000020',
    ...NAV_MODAL_CARD_STYLE,
  },
};

const {Navigator, Screen} = createStackNavigator();
const LoginNavigator = () => (
  <Navigator initialRouteName="Login">
    <Screen
      name="Login"
      component={LoginScreen}
      options={{header: () => null}}
    />
    <Screen
      name="Register"
      component={RegisterScreen}
      options={{header: () => null}}
    />
  </Navigator>
);
const mainNavigation = ({auth, subscribeChats, unsubscribeChats}) => {
  useEffect(() => {
    if (auth.user.uid) {
      subscribeChats().then(() => null);
    } else {
      unsubscribeChats();
    }
  }, [auth.user.uid]);
  return (
    <AppearanceProvider>
      <NavigationContainer>
        <Navigator
          mode={'modal'}
          initialRouteName={auth.user.uid ? 'MainTab' : 'LoginNav'}>
          <Screen
            name="LoginNav"
            component={LoginNavigator}
            options={{
              ...TransitionPresets.SlideFromRightIOS,
              header: () => null,
            }}
          />
          <Screen
            name="MainTab"
            options={{
              ...TransitionPresets.SlideFromRightIOS,
              header: () => null,
            }}
            component={MainTab}
          />
          <Screen
            name="ChatNav"
            options={{
              ...TransitionPresets.SlideFromRightIOS,
              header: () => null,
            }}
            component={ChatNavigation}
          />
          <Screen
            name="NotAuthorized"
            options={{
              ...TransitionPresets.SlideFromRightIOS,
              header: () => null,
            }}
            component={NotAuthorizedScreen}
          />
          <Screen
            name="WebViewModal"
            options={{
              ...MODAL_STACK_OPTIONS,
              title: 'WebView',
              header: () => null,
            }}>
            {props => <WebViewModal {...props} />}
          </Screen>
        </Navigator>
      </NavigationContainer>
    </AppearanceProvider>
  );
};
const mapStateToProps = state => ({
  auth: state.auth,
});
export default connect(mapStateToProps, {subscribeChats, unsubscribeChats})(
  mainNavigation,
);
