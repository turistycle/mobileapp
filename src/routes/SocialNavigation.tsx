import * as React from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import SocialScreen from "../pages/social/SocialScreen";

const {Navigator, Screen} = createStackNavigator();

export default function () {
  return (
    <Navigator initialRouteName="SocialScreen">
      <Screen
        name="SocialScreen"
        component={SocialScreen}
        options={{
          header: () => null,
        }}
      />
    </Navigator>
  );
}
