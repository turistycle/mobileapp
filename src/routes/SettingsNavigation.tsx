import * as React from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import SettingsScreen from '../pages/settings/SettingsScreen';
import UserScreen from '../pages/settings/user-screen/UserScreen';

const {Navigator, Screen} = createStackNavigator();

export default function () {
  return (
    <Navigator initialRouteName="SettingsScreen">
      <Screen
        name="SettingsScreen"
        component={SettingsScreen}
        options={{
          header: () => null,
        }}
      />
      <Screen
        name="UserScreen"
        options={{
          header: () => null,
        }}>
        {params => <UserScreen {...params} />}
      </Screen>
    </Navigator>
  );
}
