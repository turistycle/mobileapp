import * as React from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import ChatScreen from "../pages/chat/ChatScreen";
import SingleConversationScreen from "../pages/chat/single-conversation/SingleConversationScreen";

const {Navigator, Screen} = createStackNavigator();

export default function () {
  return (
    <Navigator initialRouteName="ChatScreen">
      <Screen
        name="ChatScreen"
        component={ChatScreen}
        options={{
          header: () => null,
        }}
      />
      <Screen
        name="SingleConversationScreen"
        component={SingleConversationScreen}
        options={{
          header: () => null,
        }}
      />
    </Navigator>
  );
}
