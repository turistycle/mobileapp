import * as React from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import MapScreen from '../pages/map/MapScreen';
import SummaryTripScreen from '../pages/map/summary/SummaryTripScreen';

const {Navigator, Screen} = createStackNavigator();

export default function (props) {
  return (
    <Navigator initialRouteName="MapScreen">
      <Screen
        name="MapScreen"
        options={{
          header: () => null,
        }}>
        {() => <MapScreen {...props} />}
      </Screen>
      <Screen
        name="SummaryTripScreen"
        options={{
          header: () => null,
        }}>
        {params => <SummaryTripScreen {...params} />}
      </Screen>
    </Navigator>
  );
}
