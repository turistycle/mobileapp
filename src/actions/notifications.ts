import {RESET_NOTIFICATION, SHOW_NOTIFICATION} from '../common/types';

export const showNotification = () => dispatch =>
  dispatch({type: SHOW_NOTIFICATION});

export const resetNotification = () => dispatch =>
  dispatch({type: RESET_NOTIFICATION});
