import {GOT_ERROR, GOT_MESSAGE, IS_LOADING, IS_NOT_LOADING, LOG_IN, LOG_OUT,} from '../common/types';
import {GoogleSignin, statusCodes,} from '@react-native-google-signin/google-signin';
import auth from '@react-native-firebase/auth';
import axios from 'axios';
import {AccessToken, LoginManager, Settings} from 'react-native-fbsdk-next';
import {PREFIX_URI} from '../common/consts';

export const signUpWithEmail = (
  login: string,
  displayName: string,
  password1: string,
  password2: string,
  terms: boolean,
) => async dispatch => {
  if (!login || !displayName || !password1 || !password2) {
    dispatch({
      type: GOT_ERROR,
      payload: 'Not every credential has been provided.',
    });
    return;
  }

  const re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

  if (!re.test(String(login).toLowerCase())) {
    dispatch({
      type: GOT_ERROR,
      payload: 'Incorrect email format.',
    });
    return;
  }

  if (password1 !== password2) {
    dispatch({
      type: GOT_ERROR,
      payload: "Passwords don't match.",
    });
    return;
  }

  if (!terms) {
    dispatch({
      type: GOT_ERROR,
      payload: 'Terms have to be agreed',
    });
    return;
  }

  dispatch({
    type: IS_LOADING,
  });

  await auth()
    .createUserWithEmailAndPassword(login, password1)
    .then(async ({user}) => {
      await user.updateProfile({
        displayName: displayName,
      });

      await user.reload();
      user = await auth().currentUser;
      await user.sendEmailVerification().catch(function (error) {
        console.log({fromEmailVerify: error});
      });
      return {user: user, idToken: await user.getIdToken()};
    })
    .then(({user, idToken}) => LoginWithCredential(user, idToken, dispatch))
    .then(() => auth().signOut())
    .catch(error => {
      try {
        if (error.toString().includes('[auth/email-already-in-use]'))
          dispatch({type: GOT_ERROR, payload: 'Email already in use.'});
        else dispatch({type: GOT_ERROR, payload: 'Something went wrong.'});
        console.log({error});
      } catch {
        dispatch({type: GOT_ERROR, payload: error});
      }
    });

  dispatch({
    type: IS_NOT_LOADING,
  });
};

export const signInWithEmail = (
  login: string,
  password: string,
) => async dispatch => {
  if (!login || !password) {
    dispatch({
      type: GOT_ERROR,
      payload: 'The Email or Password has not been given',
    });
    return;
  }
  dispatch({type: IS_LOADING});
  await auth()
    .signInWithEmailAndPassword(login, password)
    .then(async ({user}) => ({user, idToken: await user.getIdToken()}))
    .then(({user, idToken}) => LoginWithCredential(user, idToken, dispatch))
    .catch(error => {
      try {
        if (error.toString().includes('[auth/user-not-found]'))
          dispatch({type: GOT_ERROR, payload: 'Wrong email or password'});
        else dispatch({type: GOT_ERROR, payload: error});
      } catch {
        dispatch({type: GOT_ERROR, payload: error});
      }
    });
  dispatch({type: IS_NOT_LOADING});
};

export const signInWithGoogle = () => async dispatch => {
  GoogleSignin.configure({
    webClientId:
      '675432498642-egoms6ee16sn4pbvh70bk4luv196v0mc.apps.googleusercontent.com',
  });
  await GoogleSignin.hasPlayServices();
  GoogleSignin.signIn()
    // @ts-ignore
    .then(({accessToken, idToken}) =>
      auth.GoogleAuthProvider.credential(idToken, accessToken),
    )
    .then(credential =>
      auth()
        .signInWithCredential(credential)
        .then(async ({user}) => ({user, idToken: await user.getIdToken()}))
        .then(({user, idToken}) => LoginWithCredential(user, idToken, dispatch))
        .catch(error => {
          dispatch({type: GOT_ERROR, payload: error.message});
        }),
    )
    .catch(error => {
      if (error.code === statusCodes.SIGN_IN_CANCELLED) {
        dispatch({type: GOT_ERROR, payload: 'Cancelled '});
      } else if (error.code === statusCodes.IN_PROGRESS) {
        dispatch({type: GOT_ERROR, payload: 'Still in progress'});
      } else if (error.code === statusCodes.PLAY_SERVICES_NOT_AVAILABLE) {
        dispatch({type: GOT_ERROR, payload: 'Service is unavailable'});
      } else {
        dispatch({type: GOT_ERROR, payload: error});
      }
    });
};

export const signInWithFacebook = () => async dispatch => {
  Settings.initializeSDK();
  const result = await LoginManager.logInWithPermissions([
    'public_profile',
    'email',
  ]);
  if (result.isCancelled) {
    dispatch({type: GOT_ERROR, payload: 'Cancelled'});
    return;
  }
  const data = await AccessToken.getCurrentAccessToken();
  if (!data) {
    dispatch({type: GOT_ERROR, payload: 'Something went wrong'});
    return;
  }

  const facebookCredential = auth.FacebookAuthProvider.credential(
    data.accessToken,
  );

  auth()
    .signInWithCredential(facebookCredential)
    .then(async ({user}) => {
      console.log({user});
      return {user, idToken: await user.getIdToken()};
    })
    .then(({user, idToken}) => LoginWithCredential(user, idToken, dispatch))
    .catch(error => {
      dispatch({type: GOT_ERROR, payload: error.message});
    });
};

const LoginWithCredential = async (user, idToken, dispatch) =>
  await axios
    .post(
      `${PREFIX_URI}/auth`,
      {photoURL: user.photoURL, displayName: user.displayName},
      {
        headers: {
          Authorization: `Bearer ${idToken.toString()}`,
          Accept: 'application/json',
          'Content-Type': 'application/json',
        },
      },
    )
    .then(() => {
      dispatch({
        type: LOG_IN,
        payload: {user},
      });
    })
    .catch(error => {
      dispatch({
        type: GOT_ERROR,
        payload: error.response,
      });
    });

export const resendVerificationEmail = user => async dispatch => {
  user
    .sendEmailVerification()
    .then(() => {
      dispatch({
        type: GOT_MESSAGE,
        payload: 'Mail has been sent.',
      });
    })
    .catch(function (error) {
      console.log({fromEmailVerify: error});
    });
};

export const logOut = (callback: () => any) => dispatch => {
  dispatch({type: LOG_OUT});
  callback();
};
