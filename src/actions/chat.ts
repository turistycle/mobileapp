import firestore from '@react-native-firebase/firestore';
import auth from '@react-native-firebase/auth';
import {
  NEW_NOTIFICATION,
  RESET_CONVERSATIONS,
  RESET_CURRENT_CONVERSATION,
  SET_CURRENT_CONVERSATION,
  UPDATE_CHAT_MESSAGES,
  UPDATE_CONVERSATIONS,
  UPDATE_CONVERSATIONS_LAST_MESSAGE,
} from '../common/types';
import axios from 'axios';
import {PREFIX_URI} from '../common/consts';

let subscribeMessages;
let subscribeConversations;
let firstCallSubscribeMessages = 0;

export const subscribeChats = () => async dispatch => {
  dispatch({type: RESET_CONVERSATIONS});
  const user = await auth().currentUser;
  subscribeConversations = firestore()
    .collection('chat')
    .where('users', 'array-contains', {
      uid: user.uid,
      photoURL: user.photoURL,
      displayName: user.displayName,
    })
    .onSnapshot(snapshot => {
      snapshot.docChanges().forEach(change => {
        switch (change.type) {
          case 'added':
            dispatch({
              type: UPDATE_CONVERSATIONS,
              payload: {...change.doc.data(), id: change.doc.id},
            });
            if (firstCallSubscribeMessages > 0) {
              dispatch({
                type: NEW_NOTIFICATION,
                payload: {
                  from: change.doc.data().lastMessage.from,
                  text: change.doc.data().lastMessage.text,
                  conversationId: change.doc.id,
                },
              });
            }
            break;
          case 'modified':
            dispatch({
              type: UPDATE_CONVERSATIONS_LAST_MESSAGE,
              payload: {
                lastMessage: {...change.doc.data().lastMessage},
                id: change.doc.id,
              },
            });
            dispatch({
              type: NEW_NOTIFICATION,
              payload: {
                from: change.doc.data().lastMessage.from,
                text: change.doc.data().lastMessage.text,
                conversationId: change.doc.id,
              },
            });
            break;
          default:
            break;
        }
      });
      firstCallSubscribeMessages++;
    });
};

export const sendMessage = async (conversationId, message) => {
  const user = await auth().currentUser;
  await firestore().collection(`chat/${conversationId}/messages`).add(message);
  await firestore()
    .collection(`chat`)
    .doc(`${conversationId}`)
    .set(
      {
        lastMessage: {
          from: {
            uid: user.uid,
            photoURL: user.photoURL,
            displayName: user.displayName,
          },
          text: message.text,
          time: message.time,
        },
      },
      {merge: true},
    );
};

export const sendFirstMessage = async (
  secondUser,
  message,
  setConversationId,
) => {
  const user = await auth().currentUser;
  await firestore()
    .collection(`chat`)
    .add({
      users: [
        {uid: user.uid, photoURL: user.photoURL, displayName: user.displayName},
        {
          uid: secondUser.uid,
          photoURL: secondUser.photoURL,
          displayName: secondUser.displayName,
        },
      ],
      lastMessage: {
        from: {
          uid: user.uid,
          photoURL: user.photoURL,
          displayName: user.displayName,
        },
        text: message.text,
        time: message.time,
      },
    })
    .then(async doc => {
      setConversationId(doc.id);
      await firestore().collection(`chat/${doc.id}/messages`).add(message);
    });
};

export const subscribeCurrentChat = conversationId => async dispatch => {
  dispatch({
    type: SET_CURRENT_CONVERSATION,
    payload: conversationId,
  });
  subscribeMessages = firestore()
    .collection(`chat/${conversationId}/messages`)
    .onSnapshot(snapshot => {
      snapshot.docChanges().forEach(change => {
        if (change.type === 'added') {
          const message = change.doc.data();
          dispatch({
            type: UPDATE_CHAT_MESSAGES,
            payload: {...message, time: message.time._seconds * 1000},
          });
        }
      });
    });
};

export const unsubscribeChats = () => dispatch => {
  if (subscribeConversations) {
    subscribeConversations();
  }
  dispatch({type: RESET_CONVERSATIONS});
};

export const resetCurrentChat = () => dispatch => {
  if (subscribeMessages) {
    subscribeMessages();
  }
  dispatch({
    type: RESET_CURRENT_CONVERSATION,
  });
};

export const getAllUsers = async () => {
  const response = await axios.post(
    `${PREFIX_URI}/search`,
    {query: '', collection: 'users'},
    {
      headers: {
        Authorization: `Bearer ${await auth().currentUser.getIdToken()}`,
      },
    },
  );
  if (response.status === 200) {
    return response.data;
  }
};
