import axios from 'axios';
import {MAPBOX_TOKEN_PUBLIC} from '../common/config';
import {PREFIX_MAPBOX_URI, PREFIX_URI} from '../common/consts';
import * as turf from '@turf/turf';
import auth from '@react-native-firebase/auth';
import {GOT_ERROR, GOT_MESSAGE} from '../common/types';

export const getExternalPoints = async (): Promise<any> => {
  return await axios
    .get(`${PREFIX_URI}/markers`, {
      headers: {
        Authorization: `Bearer ${await auth().currentUser.getIdToken()}`,
      },
    })
    .then(({data}) => {
      data.forEach(x => {
        x.location = [x.longitude, x.latitude];
      });
      return data;
    });
};

export const getOptimizedRoute = async (
  coords,
  profile = 'cycling',
): Promise<any> => {
  const mappedCoords = coords.reduce(
    (acc, single) => acc + ';' + single[0] + ',' + single[1],
  );

  const response = await axios.get(
    `${PREFIX_MAPBOX_URI}/${profile}/${mappedCoords}`,
    {
      params: {
        overview: 'full',
        steps: true,
        geometries: 'geojson',
        access_token: MAPBOX_TOKEN_PUBLIC,
      },
    },
  );
  if (response.status === 200) {
    const {duration, distance} = response.data.routes[0];
    const trace = turf.featureCollection([
      turf.feature(response.data.routes[0].geometry),
    ]);

    return {
      trace,
      duration,
      distance,
      waypoints: response.data.waypoints.map((x, index) => ({
        ...x,
        waypoint_index: index,
      })),
    };
  } else {
    return {};
  }
};

export const sendMarkers = list => async dispatch => {
  return await axios
    .post(`${PREFIX_URI}/markers`, list, {
      headers: {
        Authorization: `Bearer ${await auth().currentUser.getIdToken()}`,
      },
    })
    .then(response => {
      return response.data
    })
    .catch(error => {
      console.log(error)
      dispatch({type: GOT_ERROR, payload: 'Something went wrong'});
      return null;
    });
};


export const postTrip = body => async dispatch => {
  return await axios
    .post(`${PREFIX_URI}/trips`, body, {
      headers: {
        Authorization: `Bearer ${await auth().currentUser.getIdToken()}`,
      },
    })
    .then(response => {
      console.log(response.data)
      dispatch({type: GOT_MESSAGE, payload: 'Your trip can be found in search area'});
      return true
    })
    .catch(error => {
      console.log(error)
      dispatch({type: GOT_ERROR, payload: 'Something went wrong'});
      return null;
    });
}