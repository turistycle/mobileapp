import axios from 'axios';
import {PREFIX_URI} from '../common/consts';
import auth from '@react-native-firebase/auth';
import {GOT_ERROR} from '../common/types';

export const getCommunityPosts = ({
                                    nextPage = 1,
                                    query = '',
                                  }) => async dispatch => {
  const response = await axios.get(
    `${PREFIX_URI}/users/followed?page=${nextPage}&query=${query}`,
    {
      headers: {
        Authorization: `Bearer ${await auth().currentUser.getIdToken()}`,
      },
    },
  );
  if (response.status === 200) {
    return response.data;
  } else {
    console.log(response.data);
    dispatch({type: GOT_ERROR, payload: 'Something went wrong'});
    return null;
  }
};
