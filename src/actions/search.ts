import {PREFIX_URI} from '../common/consts';
import axios from 'axios';
import {GOT_ERROR} from '../common/types';
import auth from '@react-native-firebase/auth';

export const findTrips = ({
                            query = null,
                            tags = null,
                            categories = null,
                            citiesAround = null,
                            distanceMin = 1,
                            distanceMax = 250,
                          }) => async dispatch => {
  return await axios
    .get(`${PREFIX_URI}/trips`, {
      params: {
        query,
        tags: tags?.join(','),
        categories: categories?.join(','),
        citiesAround: citiesAround?.join(','),
        distanceMin,
        distanceMax,
      },
      headers: {
        Authorization: `Bearer ${await auth().currentUser.getIdToken()}`,
      },
    })
    .then(({data}) => data)
    .catch(e => {
      console.log(e);
      dispatch({type: GOT_ERROR, payload: 'Something went wrong'});
      return [];
    });
};

export const getSingleTrip = id => async dispatch => {

  return await axios
    .get(`${PREFIX_URI}/trips/${id}`, {
      headers: {
        Authorization: `Bearer ${await auth().currentUser.getIdToken()}`,
      },
    })
    .then(({data}) => data[0])
    .catch(e => {
      console.log(e);
      dispatch({type: GOT_ERROR, payload: 'Something went wrong'});
      return [];
    });
};
