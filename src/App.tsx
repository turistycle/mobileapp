import * as React from 'react';
import {persistor, store} from './store';
import {Provider} from 'react-redux';
import Alerts from './alerts/Alerts';
import MainNavigation from './routes/MainNavigation';
import {LogBox, StatusBar} from 'react-native';
import Loading from './components/Loading';
import {PersistGate} from 'redux-persist/integration/react';
import PersistorLoading from './components/PersitorLoading';
import InnerNotification from './alerts/InnerNotification';

LogBox.ignoreLogs([
  'ReactNativeFiberHostComponent: Calling getNode() on the ref of an Animated component is no longer necessary. You can now directly use the ref instead. This method will be removed in a future release.',
]);
export default function () {
  return (
    <Provider store={store}>
      <PersistGate loading={<PersistorLoading/>} persistor={persistor}>
        <StatusBar barStyle="dark-content"/>
        <InnerNotification/>
        <Alerts/>
        <MainNavigation/>
        <Loading/>
      </PersistGate>
    </Provider>
  );
}
