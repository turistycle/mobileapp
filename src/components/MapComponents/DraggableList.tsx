import * as React from 'react';
import {useCallback, useEffect, useRef} from 'react';
import {Animated, Easing, TouchableOpacity, View} from 'react-native';
import MyText from '../MyText';
import DraggableFlatList from 'react-native-draggable-flatlist';
import Icon from "../Icon";

const styles = require('../../../assets/scss/pages/mapscreen.scss');

export const DraggableList = ({points, onChange, isShown}) => {
  const topAnimated = useRef(new Animated.Value(-500)).current;
  useEffect(() => {
    if (isShown) {
      Animated.timing(topAnimated, {
        duration: 500,
        useNativeDriver: true,
        toValue: 50,
        easing: Easing.linear,
      }).start();
    } else {
      Animated.timing(topAnimated, {
        duration: 500,
        useNativeDriver: true,
        toValue: -500,
        easing: Easing.linear,
      }).start();
    }
  }, [isShown]);

  const renderItem = useCallback(({item, index, drag, isActive}) => {
    return (
      <TouchableOpacity
        activeOpacity={1}
        style={[
          styles.draggable__point,
          isActive && styles.draggable__active,
          index === 0 && styles.draggable__point__first,
          index === points.length - 1 && styles.draggable__point__last,
        ]}
        onLongPress={drag}>
        <Icon name={"hamburger"} size={18} style={styles.draggable__point__icon} />
        <MyText>{item?.name || `Point no. ${item.waypoint_index + 1}`}</MyText>
      </TouchableOpacity>
    );
  }, []);

  return (
    <Animated.View
      style={{
        ...styles.draggable__container,
        transform: [{translateY: topAnimated}],
      }}>
      <View style={{flex: 1}}>
        {/*// @ts-ignore*/}
        <DraggableFlatList
          ItemSeparatorComponent={() => (
            <View style={{width: '100%', height: 1, backgroundColor: `#ccc`}}/>
          )}
          data={points}
          renderItem={renderItem}
          keyExtractor={(item, index) => index.toString()}
          onDragEnd={({data}) => onChange(data)}
        />
      </View>
    </Animated.View>
  );
};
