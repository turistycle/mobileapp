import * as React from 'react';
import {useEffect, useRef, useState} from 'react';
import {
  Animated,
  Dimensions,
  Easing,
  Image,
  ScrollView,
  TouchableOpacity,
  View,
} from 'react-native';
import MyText from '../MyText';
import Icon from '../Icon';
import HalfStarLeft from '../../../assets/svgs/HalfStarLeft';
import HalfStarRight from '../../../assets/svgs/HalfStarRight';
import {getRoundedAvg} from '../../helpers/math';

const styles = require('../../../assets/scss/pages/mapscreen.scss');

export const PointPopupInfo = ({
  point,
  onClose,
  onRemove,
  onAddAsWaypoint,
  isSelectedInPoints,
}) => {
  const [_point, _setPoint] = useState(point);
  const [avgRates, setAvgRates] = useState(0);
  const topAnimated = useRef(new Animated.Value(-200)).current;

  useEffect(() => {
    if (point !== null) {
      Animated.timing(topAnimated, {
        duration: 500,
        useNativeDriver: true,
        toValue: 50,
        easing: Easing.linear,
      }).start(() => _setPoint(point));
      setAvgRates(getRoundedAvg(point?.rates?.map(x => x.rate) || [0]));
    } else {
      Animated.timing(topAnimated, {
        duration: 500,
        useNativeDriver: true,
        toValue: -200,
        easing: Easing.linear,
      }).start(() => _setPoint(point));
      setAvgRates(0);
    }
    console.log({point});
  }, [point]);

  return (
    <Animated.View
      style={{
        ...styles.bottom__container,
        transform: [{translateY: topAnimated}],
      }}>
      {_point?.photos?.length > 0 && (
        <ScrollView
          style={styles.bottom__carousel}
          contentContainerStyle={{flexGrow: 1}}
          horizontal={true}
          showsHorizontalScrollIndicator={true}
          decelerationRate={0}
          persistentScrollbar={true}
          snapToInterval={Dimensions.get('window').width * 0.96}
          snapToAlignment={'center'}>
          {_point.photos.map((photo, index) => (
            <View
              key={index}
              style={[
                styles.bottom__image,
                {width: Dimensions.get('window').width * 0.96},
              ]}>
              <Image
                style={styles.bottom__image__inner}
                source={{uri: photo}}
              />
            </View>
          ))}
        </ScrollView>
      )}
      <View
        style={{
          flexDirection: 'row',
          display: _point?.photos?.length > 0 ? 'flex' : 'none',
          position: 'absolute',
          top: 150,
          right: 20,
          backgroundColor: 'rgba(0,0,0,0.2)',
        }}>
        {[1, 2, 3, 4, 5].map((val, index) => (
          <View key={index} style={{flexDirection: 'row', padding: 5}}>
            <HalfStarLeft
              size={15}
              strokeWidth={50}
              borderColor={'gold'}
              color={val - 0.5 <= avgRates ? 'gold' : 'transparent'}
            />
            <HalfStarRight
              size={15}
              strokeWidth={50}
              borderColor={'gold'}
              color={val <= avgRates ? 'gold' : 'transparent'}
            />
          </View>
        ))}
      </View>
      {_point && (
        <View style={styles.bottom__header}>
          <MyText style={styles.bottom__header__title}>
            {point?.name || `Point no. ${_point.waypoint_index + 1}`}
          </MyText>
          <View style={styles.bottom__buttons}>
            <TouchableOpacity style={styles.bottom__close} onPress={onClose}>
              <Icon name={'x'} size={20} />
            </TouchableOpacity>
            {isSelectedInPoints && (
              <TouchableOpacity
                style={styles.bottom__remove}
                onPress={() => onRemove(_point?.waypoint_index)}>
                <Icon name={'trash'} size={20} color={'red'} />
              </TouchableOpacity>
            )}
            {!isSelectedInPoints && (
              <TouchableOpacity
                style={styles.bottom__add}
                onPress={() => onAddAsWaypoint()}>
                <Icon name={'plus'} size={20} color={'green'} />
              </TouchableOpacity>
            )}
          </View>
        </View>
      )}
    </Animated.View>
  );
};
