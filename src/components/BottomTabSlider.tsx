import * as React from 'react';
import {useEffect, useState} from 'react';
import {View} from 'react-native';
import TabBarWave from '../../assets/svgs/TabBarWave';
import Icon from './Icon';
import MyText from "./MyText";


const mapNames = {
  DashboardScreen: 'Home',
  SearchNavigation: 'Search',
  MapNavigation: 'Map',
  SocialNavigation: 'People',
  SettingsNavigation: 'Prefs',
}
const styles = require('../../assets/scss/components/bottombarslider.scss');

const BottomTabSlider = ({route, focused, color}) => {
  const [iconToReturn, setIconToReturn] = useState('hamburger');

  useEffect(() => {
    switch (route.name) {
      case 'DashboardScreen':
        setIconToReturn('home');
        break;
      case 'SearchNavigation':
        setIconToReturn('search');
        break;
      case 'MapNavigation':
        setIconToReturn('pin');
        break;
      case 'SocialNavigation':
        setIconToReturn('users');
        break;
      case 'SettingsNavigation':
        setIconToReturn('hamburger');
        break;
      default:
        break;
    }
  }, []);

  return (
    <View style={[styles.tabs]}>
      <View
        style={[
          styles.tabs__wave,
          focused ? styles['tabs__wave--active'] : null,
        ]}>
        <TabBarWave
          style={[
            styles.tabs__wave__inner,
            focused ? styles['tabs__wave__inner--active'] : null,
          ]}
        />
      </View>
      {/*@ts-ignore*/}
      <Icon name={iconToReturn} strokeWidth={focused ? 5 : 0} size={focused ? 32 : 26} color={color}/>
      {focused && <MyText style={{fontSize:10, fontWeight: '500', color: 'white'}}>{mapNames[route.name]}</MyText>}
    </View>
  );
};

export default BottomTabSlider;
