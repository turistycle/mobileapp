import * as React from 'react';
import Iconmoon from 'react-native-icomoon';
import {View} from 'react-native';

const json = require('../../assets/fonts/icomoon/selection.json');

export interface IconProps {
  name:
    | 'map'
    | 'streetsign'
    | 'gears'
    | 'tools'
    | 'dial'
    | 'chat'
    | 'hamburger'
    | 'home'
    | 'pin'
    | 'search'
    | 'user'
    | 'users'
    | 'bell'
    | 'chevron-down'
    | 'find'
    | 'settings'
    | 'sliders'
    | 'map-pin'
    | 'edit'
    | 'log-out'
    | 'send'
    | 'village'
    | 'off-road'
    | 'nature'
    | 'desert'
    | 'city'
    | 'image'
    | 'repeat'
    | 'square'
    | 'x'
    | 'trash'
    | 'create'
    | 'plus'
    | 'drag';
  color?: string;
  size?: number;
  strokeWidth?: number;
  offset?: number;
  style?: any;
}

export default function Icon({name, ...restProps}: IconProps) {
  return (
    <View style={restProps.style}>
      <Iconmoon
        iconSet={json}
        name={name}
        {...restProps}
        color={restProps?.color || restProps?.style?.color || 'black'}
      />
    </View>
  );
}
