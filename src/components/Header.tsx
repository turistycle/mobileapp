import * as React from 'react';
import {SafeAreaView, TextStyle, View} from 'react-native';
import MyText from './MyText';
import {useSafeAreaInsets} from 'react-native-safe-area-context';

const styles = require('../../assets/scss/components/header.scss');

const IconPlaceholder = () => <View style={styles.header__button} />;

interface HeaderProps {
  ButtonLeft?: ({}: any) => any;
  ButtonRight?: ({}: any) => any;
  floatingHeader?: boolean;
  title: string;
  titleStyle?: TextStyle;
  darkText: boolean;
}

const Header = ({
  darkText,
  ButtonLeft = null,
  ButtonRight = null,
  title,
  titleStyle,
  floatingHeader = false,
}: HeaderProps) => {
  const insets = useSafeAreaInsets();
  return (
    <Floating isFloating={floatingHeader}>
      <View
        style={[
          styles.header__inner,
          floatingHeader && {
            ...styles['header__inner--floating'],
            top: insets.top > 0 ? insets.top : 5,
          },
        ]}>
        {ButtonLeft == null ? (
          <IconPlaceholder />
        ) : (
          <ButtonLeft
            style={{
              ...styles,
              header__buttonIcon: {
                ...styles.header__buttonIcon,
                ...(darkText ? styles['header__buttonIcon--dark'] : {}),
              },
            }}
          />
        )}
        <MyText
          style={[
            titleStyle,
            {
              ...styles.header__title,
              ...(darkText ? styles['header__title--dark'] : {}),
            },
          ]}>
          {title}
        </MyText>
        {ButtonRight == null ? (
          <IconPlaceholder />
        ) : (
          <ButtonRight
            style={{
              ...styles,
              header__buttonIcon: {
                ...styles.header__buttonIcon,
                ...(darkText ? styles['header__buttonIcon--dark'] : {}),
              },
            }}
          />
        )}
      </View>
    </Floating>
  );
};

const Floating = ({isFloating, children}) =>
  isFloating ? (
    <View style={styles.header}>{children}</View>
  ) : (
    <SafeAreaView style={styles.header}>{children}</SafeAreaView>
  );

export default Header;
