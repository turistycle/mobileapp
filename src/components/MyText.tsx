import * as React from 'react';
import {Text, TextProps} from 'react-native';

interface MyTextProps extends TextProps {
  children: any
}

const MyText = (props: MyTextProps) => (
  <Text
    {...props}
    style={[
      {
        color: '#474747',
        fontFamily: 'Montserrat',
      },
      props.style,
    ]}>
    {props.children}
  </Text>
);

export default MyText;
