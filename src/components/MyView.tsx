import * as React from 'react';
import {useCallback} from 'react';
import {ImageBackground, SafeAreaView, View} from 'react-native';
const bottomClouds = require('../../assets/images/bottom-clouds.png');
import LinearGradient from 'react-native-linear-gradient';
import Header from './Header';

const styles = require('../../assets/scss/components/myview.scss');

const MyView = ({
                  title = '',
                  titleStyle = {},
                  ButtonLeft = null,
                  ButtonRight = null,
                  children = null,
                  floatingHeader = false,
                  withoutHeader = false,
                  withoutMargin = false,
                  withoutBackground = false,
                  style = null,
                  withoutSafeArea = false,
                }) => {
  const MemorizedHeader = useCallback(
    ({ButtonLeft, ButtonRight, titleStyle, title, floatingHeader}) => (
      <Header
        darkText={withoutBackground || floatingHeader}
        floatingHeader={floatingHeader}
        ButtonLeft={ButtonLeft}
        ButtonRight={ButtonRight}
        titleStyle={titleStyle}
        title={title}
      />
    ),
    [],
  );
  return (
    <BackgroundCheck
      withoutSafeArea={withoutSafeArea}
      withoutBackground={withoutBackground}>
      {!withoutHeader ? (
        <MemorizedHeader
          floatingHeader={floatingHeader}
          ButtonLeft={ButtonLeft}
          ButtonRight={ButtonRight}
          titleStyle={{...styles.titleText, ...titleStyle}}
          title={title}
        />
      ) : null}
      <View
        style={[styles.container, {margin: withoutMargin ? 0 : '3%'}, style]}>
        {children}
      </View>
    </BackgroundCheck>
  );
};

const SafeAreaCheck = ({children, withoutSafeArea, style = null}) => {
  return withoutSafeArea ? (
    <View style={style}>{children}</View>
  ) : (
    <SafeAreaView style={style}>{children}</SafeAreaView>
  );
};

const BackgroundCheck = ({withoutBackground, withoutSafeArea, children}) => {
  return withoutBackground ? (
    <SafeAreaCheck withoutSafeArea={withoutSafeArea} style={{flex: 1}}>
      {children}
    </SafeAreaCheck>
  ) : (
    <LinearGradient
      colors={[styles.gradient.color1, styles.gradient.color2]}
      start={{x: 0, y: 0}}
      end={{x: -0.5, y: 0.8}}
      locations={[0, 1]}
      style={{flex: 1}}>
      <SafeAreaCheck
        withoutSafeArea={withoutSafeArea}
        style={{width: '100%', height: '102%'}}>
        <ImageBackground
          source={null}
          style={styles.background}
          imageStyle={styles.background__image}>
          {children}
        </ImageBackground>
      </SafeAreaCheck>
    </LinearGradient>
  );
};
export default MyView;
