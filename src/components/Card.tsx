import * as React from 'react';
import {View} from 'react-native';
import MyText from './MyText';
import Icon from './Icon';

const styles = require('../../assets/scss/components/card.scss');

const Card = ({
  headerText,
  headerIcon = null,
  headerStyle = null,
  children = null,
  style = null,
  bodyStyle = null,
}) => {
  return (
    <View style={[styles.card, style]}>
      {`${headerText}` ? (
        <View style={headerStyle}>
          {headerIcon && (
            <Icon name={headerIcon} size={15} style={styles.card__headerIcon} />
          )}
          <MyText style={[styles.card__headerText, headerStyle]}>
            {headerText}
          </MyText>
        </View>
      ) : (
        headerText
      )}
      <View style={[styles.card__body, bodyStyle]}>{children}</View>
    </View>
  );
};

export default Card;
