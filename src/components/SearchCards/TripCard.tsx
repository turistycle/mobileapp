import * as React from 'react';
import {useEffect, useState} from 'react';
import {
  ActivityIndicator,
  TouchableOpacity,
  View,
  ViewStyle,
} from 'react-native';
import MyText from '../MyText';
import MapboxGL from '@react-native-mapbox-gl/maps';
import MapView from '../MapView';
import Icon from '../Icon';

const styles = require('../../../assets/scss/components/searchcard.scss');

const mapCats = {
  offRoad: {icon: 'off-road', color: `#ad6644`},
  nature: {icon: 'nature', color: `#4a9320`},
  forest: {icon: 'desert', color: `#209363`},
  city: {icon: 'city', color: `#ccc`},
};

interface SearchCardProps {
  item: any;
  style?: ViewStyle;
  navigateToDetails: (item: any, bounds: any) => any;
}

const TripCard = ({item, style = null, navigateToDetails}: SearchCardProps) => {
  const [isLoading, setIsLoading] = useState(true);
  const [bounds, setBounds] = useState(null);
  const getMaxBounds = () => {
    try {
      const coords = item.geoJson.features[0].geometry.coordinates;
      const ne: [number, number] = [
        Math.max(...coords.map(x => x[0])),
        Math.max(...coords.map(x => x[1])),
      ];
      const sw: [number, number] = [
        Math.min(...coords.map(x => x[0])),
        Math.min(...coords.map(x => x[1])),
      ];
      setBounds({
        ne,
        sw,
        paddingLeft: 10,
        paddingRight: 10,
        paddingTop: 10,
        paddingBottom: 10,
      });
    } catch {
      setBounds({
        ne: [0, 0],
        sw: [0, 0],
        paddingLeft: 10,
        paddingRight: 10,
        paddingTop: 10,
        paddingBottom: 10,
      });
    }
  };

  useEffect(() => {
    getMaxBounds();
    setIsLoading(false);
  }, []);

  return (
    <TouchableOpacity
      onPress={() =>
        navigateToDetails({name: item.name, _id: item._id}, bounds)
      }
      style={[styles.card, style]}
      activeOpacity={0.8}>
      {isLoading ? (
        <View
          style={{
            ...styles.map,
            alignItems: 'center',
            justifyContent: 'center',
          }}>
          <ActivityIndicator size={20} color={'black'} />
        </View>
      ) : (
        <MapView style={styles.map}>
          <MapboxGL.Camera bounds={bounds} animationDuration={0} />
          <MapboxGL.ShapeSource id="line" shape={item.geoJson}>
            <MapboxGL.LineLayer id="lineLayer" style={styles.map__line} />
          </MapboxGL.ShapeSource>
          <MapboxGL.PointAnnotation
            id={'first'}
            coordinate={item.geoJson.features[0].geometry.coordinates[0]}>
            <View style={styles.map__point} />
          </MapboxGL.PointAnnotation>
          <MapboxGL.PointAnnotation
            id={'second'}
            coordinate={
              item.geoJson.features[0].geometry.coordinates[
                item.geoJson.features[0].geometry.coordinates.length - 1
              ]
            }>
            <View style={styles.map__point} />
          </MapboxGL.PointAnnotation>
        </MapView>
      )}
      <View style={styles.firstRow}>
        <View style={styles.column}>
          <MyText style={styles.firstRow__mainText}>{item.name}</MyText>
        </View>
      </View>
      <RowInfo list={item.categories} label={'categories'} />
    </TouchableOpacity>
  );
};

const RowInfo = ({list, label}) => {
  return !list.length ? null : (
    <View style={styles.secondRow}>
      <MyText style={styles.secondRow__label}>{label}</MyText>
      {list.map((item, index) =>
        mapCats[item] ? (
          <Icon
            key={index}
            name={mapCats[item].icon}
            size={20}
            style={{color: mapCats[item].color, paddingRight: 10}}
          />
        ) : (
          <MyText style={styles.secondRow__value}>{item}</MyText>
        ),
      )}
    </View>
  );
};

export default TripCard;
