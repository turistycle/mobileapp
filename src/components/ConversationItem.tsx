import MyText from './MyText';
import * as React from 'react';
import {connect} from 'react-redux';
import {Image, TouchableOpacity, View} from 'react-native';

const styles = require('../../assets/scss/components/conversationitem.scss');
const Avatar = require('../../assets/images/avatar.png');

const ConversationItem = ({conversation, user, onPress}) => {
  const contrUser = conversation.users.find(x => x.uid !== user.uid);
  return (
    <TouchableOpacity
      onPress={() => onPress(contrUser)}
      style={styles.conversation}>
      <Image
        source={!contrUser.photoURL ? Avatar : {uri: contrUser.photoURL}}
        style={styles.conversation__image}
      />
      <View style={styles.conversation__right}>
        <MyText style={styles.conversation__name}>
          {contrUser.displayName}
        </MyText>
        <MyText style={styles.conversation__message}>
          {(conversation.lastMessage.from.uid === user.uid ? 'You: ' : '') +
          conversation.lastMessage.text}
        </MyText>
      </View>
    </TouchableOpacity>
  );
};
const mapStateToProps = state => ({
  user: state.auth.user,
});
export default connect(mapStateToProps)(ConversationItem);
