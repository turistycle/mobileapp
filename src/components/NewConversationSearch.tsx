import * as React from 'react';
import {useEffect, useState} from 'react';
import {ActivityIndicator, Image, ScrollView, TextInput, TouchableOpacity, View} from 'react-native';
import MyView from './MyView';
import {connect} from 'react-redux';
import Icon from './Icon';
import {getAllUsers} from '../actions/chat';
import MyText from './MyText';

const styles = require('../../assets/scss/components/newconversationsearch.scss');

const Avatar = require('../../assets/images/avatar.png');

const NewConversationSearch = ({
                                 onDismiss,
                                 navigation,
                                 user,
                                 conversations,
                               }) => {
  const [isLoading, setIsLoading] = useState(true);
  const [usersList, setUsersList] = useState([]);
  const [searchUser, setSearchUser] = useState('');
  useEffect(() => {
    getAllUsers().then(data => {
      setUsersList(data);
      setIsLoading(false);
    });
  }, []);
  const checkConversationExistence = secondUser => {
    const searchConversation = conversations.find(conv => {
      const myUser = conv.users.find(x => x.uid === user.uid);
      if (myUser) {
        const friend = conv.users.find(x => x.uid === secondUser.uid);
        if (friend) return true;
      }
      return false;
    });
    navigation.navigate('SingleConversationScreen', {
      conversationId: searchConversation ? searchConversation.id : null,
      secondUser,
    });
  };

  return (
    <MyView withoutBackground withoutHeader withoutMargin style={styles.whole}>
      <View style={styles.search}>
        <View style={styles.search__left}>
          <TouchableOpacity
            onPress={onDismiss}
            style={{...styles.search__button, transform: [{rotate: '90deg'}]}}>
            <Icon
              name={'chevron-down'}
              size={30}
              style={styles.search__buttonIcon}
            />
          </TouchableOpacity>
          <TextInput
            onChangeText={setSearchUser}
            value={searchUser}
            style={styles.search__input}
            placeholder="Search conversation..."
            placeholderTextColor="#999"
          />
        </View>
        {searchUser !== '' && (
          <TouchableOpacity>
            <Icon
              style={styles.search__dismiss}
              size={15}
              name="gears"
            />
          </TouchableOpacity>
        )}
      </View>
      {isLoading ? (
        <ActivityIndicator size={20} color={'black'}/>
      ) : usersList.length === 0 ? (
        <MyText>Cannot display users</MyText>
      ) : (
        <ScrollView>
          {usersList.filter(user => user.displayName.toLowerCase().match(searchUser.trim().toLowerCase())).map((user, index) => (
            <TouchableOpacity
              key={index}
              onPress={() => checkConversationExistence({...user})}
              style={styles.user}>
              <Image
                source={!user.photoURL ? Avatar : {uri: user.photoURL}}
                style={styles.user__image}
              />
              <View>
                <MyText style={styles.user__name}>{user.displayName}</MyText>
              </View>
            </TouchableOpacity>
          ))}
        </ScrollView>
      )}
    </MyView>
  );
};
const mapStateToProps = state => ({
  conversations: state.chat.conversations,
  user: state.auth.user,
});
export default connect(mapStateToProps)(NewConversationSearch);
