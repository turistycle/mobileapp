import * as React from 'react';
import LinearGradient from 'react-native-linear-gradient';
import {Dimensions, Image} from 'react-native';
import MyText from './MyText';

const logo = require('../../assets/images/logoWhiteBig.png');
const styles = require('../../assets/scss/components/persistorloading.scss');
const PersistorLoading = () => {
  const imageWidth = Dimensions.get('window').width * 0.94;
  const imageHeight = imageWidth * 0.52483962404;
  return (
    <LinearGradient
      colors={['#91BD55', '#009651']}
      start={{x: 1.2, y: 0}}
      end={{x: -0.5, y: 1}}
      locations={[0, 1]}
      style={styles.loading}>
      <Image
        source={logo}
        style={{...styles.image, width: imageWidth, height: imageHeight}}
      />
      <MyText style={styles.title}>TURISTYCLE</MyText>
    </LinearGradient>
  );
};

export default PersistorLoading;
