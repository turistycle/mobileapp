import * as React from 'react';
import {useState} from 'react';
import {ScrollView, TouchableOpacity, View} from 'react-native';
import Card from './Card';
import MyText from './MyText';
import Slider from '@ptomasroos/react-native-multi-slider';
import {allFilters, initialFiltersState} from '../common/consts';

const styles = require('../../assets/scss/components/filtersmodal.scss');



const FiltersModal = ({currentFilters, updateFilters, clearFilters}) => {
  const [filters, setFilters] = useState(currentFilters);

  const checkFilterOption = (filter, value) => {
    const check = filters[filter].find(x => x === value.value);
    if (check) {
      setFilters({
        ...filters,
        [filter]: [...filters[filter].filter(x => x !== value.value)],
      });
    } else {
      setFilters({
        ...filters,
        [filter]: [...filters[filter], value.value],
      });
    }
  };
  const checkDifference = () => {
    return JSON.stringify(filters) !== JSON.stringify(currentFilters);
  };

  const checkClear = () => {
    return JSON.stringify(filters) !== JSON.stringify(initialFiltersState);
  };
  return (
    <View style={styles.container}>
      <View style={styles['row-between']}>
        <TouchableOpacity
          onPress={() => updateFilters(filters)}
          style={styles['row-between__button']}>
          <MyText
            style={[
              styles['row-between__button__apply'],
              !checkDifference() && styles['row-between__button--disabled'],
            ]}>
            Apply
          </MyText>
        </TouchableOpacity>
        <TouchableOpacity
          onPress={clearFilters}
          style={styles['row-between__button']}>
          <MyText
            style={[
              styles['row-between__button__clear'],
              !checkClear() && styles['row-between__button--disabled'],
            ]}>
            Clear
          </MyText>
        </TouchableOpacity>
      </View>
      <MyText style={[styles.filter__title, styles['filter__title--padding']]}>
        Trip length
      </MyText>
      <Slider
        values={[filters.distanceMin, filters.distanceMax]}
        min={1}
        max={250}
        customMarker={CustomMarker}
        onValuesChange={val =>
          setFilters({...filters, distanceMin: val[0], distanceMax: val[1]})
        }
        step={1}
        allowOverlap
        snapped
        containerStyle={styles.filter__slider}
      />
      {allFilters.map(({label, values, filter}, index) => (
        <View key={index} style={styles.filter}>
          <MyText style={styles.filter__title}>{label}</MyText>
          <ScrollView style={styles.filter__options} horizontal>
            {values.map((item, index) => (
              <TouchableOpacity
                onPress={() => checkFilterOption(filter, item)}
                key={index}
                activeOpacity={0.8}>
                <Card
                  headerText={item.label}
                  headerIcon={item.icon}
                  headerStyle={{
                    color: item.color === '#f5f5f5' ? 'black' : `white`,
                    flexDirection: 'row',
                  }}
                  style={[
                    styles.filter__card,
                    filters[filter].find(x => x === item.value) && {opacity: 1},
                    {backgroundColor: item.color},
                    index === 0 && {marginLeft: 15},
                    index === values.length - 1 && {marginRight: 15},
                  ]}
                  bodyStyle={styles.filter__card__body}
                />
              </TouchableOpacity>
            ))}
          </ScrollView>
        </View>
      ))}
    </View>
  );
};

const CustomMarker = e => {
  return (
    <View
      style={{
        backgroundColor: '#5b8ecb',
        padding: 10,
        borderRadius: 30,
        alignItems: 'center',
      }}>
      <MyText style={{position: 'absolute', top: -20}}>{e.currentValue}</MyText>
    </View>
  );
};

export default FiltersModal;
