import MyText from './MyText';
import * as React from 'react';
import {useEffect, useState} from 'react';
import {ActivityIndicator, Image, TouchableOpacity, View} from 'react-native';
import {postDate, postTime} from '../helpers/dates';
import Icon from './Icon';
import {connect} from 'react-redux';
import MapView from './MapView';
import MapboxGL from '@react-native-mapbox-gl/maps';
import {getOptimizedRoute} from '../actions/maps';

const styles = require('../../assets/scss/components/socialpostitem.scss');

const SocialPostItem = ({
  item: {uid, trip, displayName, photoURL, statistics, startDate},
  navigation,
  conversations,
  user,
}) => {
  const [isLoading, setIsLoading] = useState(true);
  const [geoJson, setGeoJson] = useState(null);
  const [bounds, setBounds] = useState<{
    ne: [number, number];
    sw: [number, number];
    paddingLeft?: number;
    paddingRight?: number;
    paddingTop?: number;
    paddingBottom?: number;
  }>(null);
  const chatWithAuthor = () => {
    const searchConversation = conversations.find(conv => {
      const myUser = conv.users.find(x => x.uid === user.uid);
      if (myUser) {
        const friend = conv.users.find(x => x.uid === uid);
        if (friend) return true;
      }
      return false;
    });
    navigation.navigate(
      'ChatNav',
      {},
      setTimeout(
        () =>
          navigation.replace('SingleConversationScreen', {
            conversationId: searchConversation ? searchConversation.id : null,
            secondUser: {
              uid,
              displayName,
              photoURL,
            },
          }),
        1,
      ),
    );
  };

  const getMaxBounds = coords => {
    const ne: [number, number] = [
      Math.max(...coords.map(x => x[0])),
      Math.max(...coords.map(x => x[1])),
    ];
    const sw: [number, number] = [
      Math.min(...coords.map(x => x[0])),
      Math.min(...coords.map(x => x[1])),
    ];
    setBounds({
      ne,
      sw,
      paddingLeft: 20,
      paddingRight: 20,
      paddingTop: 20,
      paddingBottom: 20,
    });
  };

  useEffect(() => {
    setGeoJson(trip.geoJson);
    getMaxBounds(trip.geoJson.features[0].geometry.coordinates);
    setIsLoading(false);
  }, []);
  return (
    <View style={styles.post}>
      <View style={styles.top}>
        <View style={styles.user}>
          <Image style={styles.user__image} source={{uri: photoURL}} />
          <View style={styles.user__info}>
            <MyText style={styles.user__info__text}>{displayName}</MyText>
            <MyText style={styles.user__info__date}>
              {postDate(startDate)}
            </MyText>
          </View>
        </View>
        <TouchableOpacity style={styles.chat} onPress={chatWithAuthor}>
          <Icon style={styles.chat__icon} name={'chat'} size={30} />
        </TouchableOpacity>
      </View>
      {isLoading ? (
        <View style={styles.map}>
          <ActivityIndicator size={20} color={'black'} />
        </View>
      ) : (
        <MapView style={styles.map}>
          <MapboxGL.Camera bounds={bounds} animationDuration={0} />
          <MapboxGL.ShapeSource id="line" shape={trip.geoJson}>
            <MapboxGL.LineLayer id="lineLayer" style={styles.map__line} />
          </MapboxGL.ShapeSource>
          {[
            trip.geoJson.features[0].geometry.coordinates[0],
            trip.geoJson.features[0].geometry.coordinates[
              trip.geoJson.features[0].geometry.coordinates.length - 1
            ],
          ].map((x, index) => (
            <MapboxGL.PointAnnotation
              key={index}
              id={'point ' + index}
              coordinate={x}>
              <View style={styles.map__point} />
            </MapboxGL.PointAnnotation>
          ))}
        </MapView>
      )}
      <View style={styles.stats}>
        <MyText style={styles.stats__text}>
          {statistics.avgSpeed + ' km/h'}
        </MyText>
        <View style={styles.stats__line} />
        <MyText style={styles.stats__text}>{postTime(statistics.time)}</MyText>
      </View>
    </View>
  );
};
const mapStateToProps = state => ({
  conversations: state.chat.conversations,
  user: state.auth.user,
});
export default connect(mapStateToProps)(SocialPostItem);
