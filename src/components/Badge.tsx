import * as React from 'react';
import {View} from 'react-native';
import MyText from './MyText';

const styles = require('../../assets/scss/components/badge.scss');

const Badge = ({value = 3}) => {
  return (
    <View style={styles.badge}>
      <MyText style={styles.badge__text}>{value}</MyText>
    </View>
  );
};

export default Badge;
