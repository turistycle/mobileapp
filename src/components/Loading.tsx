import * as React from 'react';
import {useEffect, useRef} from 'react';
import {Animated, Easing, View} from 'react-native';
import {connect} from 'react-redux';

const styles = require('../../assets/scss/components/loading.scss');

const Loading = ({isLoading}) => {
  const fadeAnim1 = useRef(new Animated.Value(0)).current;
  const fadeAnim2 = useRef(new Animated.Value(0)).current;

  useEffect(() => {
    Animated.loop(
      Animated.sequence([
        Animated.timing(fadeAnim1, {
          toValue: 1,
          duration: 350,
          easing: Easing.linear,
          useNativeDriver: true,
        }),
        Animated.timing(fadeAnim2, {
          toValue: 1,
          duration: 300,
          easing: Easing.linear,
          useNativeDriver: true,
        }),
        Animated.timing(fadeAnim1, {
          toValue: 2,
          duration: 350,
          easing: Easing.linear,
          useNativeDriver: true,
        }),
        Animated.timing(fadeAnim2, {
          toValue: 2,
          duration: 300,
          easing: Easing.linear,
          useNativeDriver: true,
        }),
        Animated.timing(fadeAnim1, {
          toValue: 0,
          duration: 0,
          easing: Easing.linear,
          useNativeDriver: true,
        }),
      ]),
    ).start();
  }, [isLoading]);

  const spin = fadeAnim1.interpolate({
    inputRange: [0, 1, 2],
    outputRange: ['0deg', '180deg', '360deg'],
  });
  const spinInner = fadeAnim2.interpolate({
    inputRange: [0, 1, 2],
    outputRange: ['0%', '-49%', '49%'],
  });

  return !isLoading ? null : (
    <View style={styles.isLoading}>
      <Animated.View
        style={{
          ...styles.loader,
          transform: [{rotate: spin}],
        }}>
        <View
          style={{
            ...styles.loader__inner,
          }}
        />
        <Animated.View
          style={{
            ...styles.loader__inner,
            transform: [{translateX: spinInner}],
          }}
        />
        <View
          style={{
            ...styles.loader__inner,
          }}
        />
      </Animated.View>
    </View>
  );
};

const mapStateToProps = state => ({
  isLoading: state.loading,
});

export default connect(mapStateToProps)(Loading);
