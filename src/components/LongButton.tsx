import * as React from 'react';
import MyText from './MyText';
import {TouchableOpacity} from 'react-native';
import Icon, {IconProps} from './Icon';

const styles = require('../../assets/scss/components/longbutton.scss');

export interface LongButtonProps {
  title: string;
  icon: IconProps['name'];
  onPress: (...args) => any;
  style?: any;
}

const LongButton = ({title, icon, onPress, style}: LongButtonProps) => {
  return (
    <TouchableOpacity style={styles.button} onPress={onPress}>
      <MyText style={styles.button__text}>{title}</MyText>
      <Icon style={{...styles.button__icon, ...style}} size={20} name={icon}/>
    </TouchableOpacity>
  );
};

export default LongButton;
