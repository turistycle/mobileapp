import * as React from 'react';
import MapboxGL from '@react-native-mapbox-gl/maps';
import {MAPBOX_TOKEN} from '../common/config';
import {View} from 'react-native';

const styles = require('../../assets/scss/components/mapview.scss');

MapboxGL.setAccessToken(MAPBOX_TOKEN);

interface MapViewProps {
  style?: any;
  children?: any;
  disableInteraction?: boolean;
  onPress?: (any) => any
}

const MapView = ({
                   style,
                   children = null,
                   disableInteraction = true,
                   onPress = (_) => null,
                 }: MapViewProps) => {
  return (
    <View style={{...style}}>
      <MapboxGL.MapView
        styleURL={'mapbox://styles/czarcza1/cktkfrqkw836l17ppo7qb65b2'}
        style={[
          styles.map,
          style?.borderRadius
            ? {borderRadius: style.borderRadius, overflow: 'hidden'}
            : {},
        ]}
        // userTrackingMode={UserTrackingModes.Follow}
        zoomEnabled={true}
        onPress={onPress}>
        {children}
      </MapboxGL.MapView>
      {disableInteraction && (
        <View
          style={{
            position: 'absolute',
            top: 0,
            bottom: 0,
            width: '100%',
            height: '100%',
          }}
        />
      )}
    </View>
  );
};
export default MapView;
