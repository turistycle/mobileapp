// For React Native 0.59 and above
const upstreamTransformer = require('metro-react-native-babel-transformer');
const sassTransformator = require('react-native-sass-transformer');

const vueNativeScripts = require('vue-native-scripts');
const vueExtensions = ['vue'];

module.exports.transform = function ({src, filename, options}) {
  if (['sass', 'scss'].some(ext => filename.endsWith('.' + ext))) {
    return sassTransformator.transform({src, filename, options});
  }
  if (vueExtensions.some(ext => filename.endsWith('.' + ext))) {
    return vueNativeScripts.transform({src, filename, options});
  }
  return upstreamTransformer.transform({src, filename, options});
};
